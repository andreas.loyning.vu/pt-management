import Vue from "vue";
import Router from "vue-router";
import store from "./../store";
import auth from "./../controllers/auth.controller";
import swal from "sweetalert2";

import Main from "./../pages/Main";
import Login from "./../pages/Login";
import Register from "./../pages/Register";

import Dashboard from "./../pages/Dashboard";
import Profile from "./../pages/Profile";
import Invoices from "../pages/Invoices";
import Venues from "../pages/Venues";
import Trainers from "./../pages/Trainers";
import Clients from "./../pages/Clients";
import ClientRegistrasion from "../pages/ClientRegistrasion";
import Client from "../pages/Client";
import Report from "./../pages/Report";
import Booking from "../components/Booking/Bookings";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      component: Main,
      async beforeEnter(to, from, next) {
        if (store.getters.getAuthUser) {
          next();
        } else if (localStorage.getItem("token")) {
          await store.dispatch("resumeSession");
          next();
        } else {
          next("/login");
        }
      },
      children: [
        {
          path: "/trainers",
          component: Trainers,
          beforeEnter(to, from, next) {
            let user = store.getters.getAuthUser;
            if (user.role === "Client") {
              next("/profile");
            } else {
              next();
            }
          }
        },
        {
          path: "/clients",
          component: Clients,
          beforeEnter(to, from, next) {
            let user = store.getters.getAuthUser;
            if (user.role === "Client") {
              next("/profile");
            } else {
              next();
            }
          }
        },
        {
          path: "/reports",
          component: Report,
          beforeEnter(to, from, next) {
            let user = store.getters.getAuthUser;
            if (user.role === "Client") {
              next("/profile");
            } else {
              next();
            }
          }
        },

        {
          path: "/profile",
          component: Profile
        },
        {
          path: "/",
          component: Dashboard,
          beforeEnter(to, from, next) {
            let user = store.getters.getAuthUser;
            if (user.role === "Client") {
              next("/profile");
            } else {
              next();
            }
          }
        },
        {
          path: "/venues",
          component: Venues,
          beforeEnter(to, from, next) {
            let user = store.getters.getAuthUser;
            if (user.role === "Client") {
              next("/profile");
            } else {
              next();
            }
          }
        },
        {
          path: "/bookings",
          component: Booking,
          beforeEnter(to, from, next) {
            let user = store.getters.getAuthUser;
            if (user.role === "Client") {
              next("/profile");
            } 
            else if(user.role === "Trainer"){
              next("")
            }
            else {
              next();
            }
          }
        },
        {
          path: "/invoices",
          component: Invoices,
          beforeEnter(to, from, next) {
            let user = store.getters.getAuthUser;
            if (user.role === "Client") {
              next("/profile");
            } else {
              next();
            }
          }
        },
        {
          path: "/client/:id",
          component: Client,
          beforeEnter(to, from, next) {
            let user = store.getters.getAuthUser;
            if (user.role === "Client") {
              next("/profile");
            } else {
              next();
            }
          }
        }
      ]
    },
    {
      path: "/login",
      component: Login,
      async beforeEnter(to, from, next) {
        if (localStorage.getItem("token")) {
          store.dispatch("resumeSession");
          next("/");
        } else if (store.getters.getToken) {
          next("/");
        } else {
          next();
        }
      }
    },
    {
      path: "/register",
      component: Register,
      async beforeEnter(to, from, next) {
        if (store.getters.getAuthUser) {
          next("/");
        } else if (localStorage.getItem("token")) {
          await store.dispatch("resumeSession");
          next("/");
        } else {
          next();
        }
      }
    },
    {
      path: "/register/:code",
      component: ClientRegistrasion,
      async beforeEnter(to, from, next) {
        if (await auth.codeIsActive(to.params.code)) {
          next();
        } else {
          next("/login");
          swal.fire({
            icon: "error",
            title: "Code is invalid",
            timer: 2000
          });
        }
      }
    }
  ]
});
