import Vue from "vue";
import App from "./App.vue";

import router from "./router";
import store from "./store";
import GAuth from "vue-google-oauth2";
import vuetify from '@/plugins/vuetify' // path to vuetify export

Vue.config.productionTip = false;

Vue.config.productionTip = false;
const gauthOption = {
  clientId:
    "534387892028-dq5djq820le57mmh4ggmsh233gjnqiel.apps.googleusercontent.com",
  prompt: "select_account",
  fetch_basic_profile: true
};
Vue.use(GAuth, gauthOption);

new Vue({
  vuetify,
  store: store,
  router: router,
  render: h => h(App)
}).$mount("#app");
