import axios from "../utils/axios";

export default {
  async getInvoices() {
    const response = await axios.get("/Invoices");
    return response.data;
  },
  async getTrainersInvoices(id) {
    const response = await axios.get(`/Trainers/${id}/invoices`);
    return response.data;
  },
  async getInvoiceLinesForInvoice(id) {
    const response = await axios.get(`/Invoices/${id}/invoicelines`);
    return response.data;
  },
  async postInvoice(invoice) {
    const response = await axios.post("/Invoices", invoice);
    return response.data;
  },
  async postInvoiceLines(invoiceLine) {
    const response = await axios.post("/InvoiceLines", invoiceLine);
    return response.data;
  },
  sendInvoiceByMail(id, email) {
    console.log(id);
    console.log(email);
    return axios.get(`/Mail/invoice/${id}`, {
      params: {
        email: email
      }
    });
  },
  getInvoicesDetail() {
    return axios.get("/invoices/details");
  }
};
