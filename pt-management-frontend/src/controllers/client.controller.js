import axios from "../utils/axios";

export default {
    getAllClients(){
        return axios.get("/clients");
    },
    getClient(id){
        return axios.get(`/clients/${id}`);
    }
}
