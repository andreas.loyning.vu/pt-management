import axios from "../utils/axios";
export default {
  async getTrainersClients(id) {
    const response = await axios.get(`/Trainers/${id}/clients`);
    return response.data;
  },

  async getTrainers() {
    const response = await axios.get(`/Trainers`);
    return response.data;
  },

  async putTrainers(id, trainer) {
    const response = await axios.put(`/Trainers/${id}`, trainer);
    return response;
  },

  updateTrainerStatus(id, status) {
    return axios.put(`/trainers/${id}/active/${status}`);
  },

  enableTrainer(id) {
    return axios.put(`/trainers/${id}/enable`);
  }
};
