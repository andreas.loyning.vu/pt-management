import axios from "./../utils/axios";

export default {
  getAuthUser() {
    return axios.get("/login");
  },
  updateTrainer(trainer) {
    return axios.put(`/trainers/${trainer.trainerId}`, trainer);
  },
  getTrainer(trainerId) {
    return axios.get(`/trainers/${trainerId}`);
  },
  getClient(clientId) {
    return axios.get(`clients/${clientId}`);
  },
  updateClient(client) {
    console.log(client);
    return axios.put(`clients/${client.clientId}`, client);
  },
  getClientsWeights(clientId) {
    return axios.get(`weights/client/${clientId}`);
  },
  async postNewWeight(clientId, weight) {
    let body = {
      clientId,
      date: new Date().toJSON(),
      weightAtTime: weight
    };
    console.log(body);
    return axios.post(`weights`, body);
  },

  getAllClients() {
    return axios.get("clients");
  }
};
