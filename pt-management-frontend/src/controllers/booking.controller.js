import axios from "../utils/axios";

export default {
    async postBooking(booking){
       const response = await axios.post("/Bookings", booking);
       return response; 
    },
    async getTrainerBookings(id){
        const response = await axios.get(`bookings/trainer/${id}`);
        return response.data;
    },
    async updateBooking(booking){
        const response = await axios.put(`/bookings/${booking.bookingId}`, booking);
        return response;
    },
    async getClientBookings(id){
        const response = await axios.get(`/bookings/client/${id}`);
        return response.data;
    },
    async getAllBookings(){
        const response = await axios.get("/bookings");
        return response.data;
    }
}