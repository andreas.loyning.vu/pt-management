import axios from "./../utils/axios";

export default {
  getVenues() {
    return axios.get("/Venues");
  },
  getActiveVenues() {
    return axios.get("Venues/active");
  },
  putVenue(venueId, venue) {
    console.log(venueId, venue);
    return axios.put(`/Venues/${venueId}`, venue);
  },
  postVenue(venue) {
    console.log(venue);
    return axios.post(`/Venues`, venue);
  },
  getVenue(id) {
    return axios.get(`/Venues/${id}`);
  },
  getVenuefromUrl(url) {
    return axios.get(url);
  }
};
