/* eslint-disable no-unreachable */
import axios from "./../utils/axios";
import store from "./../store";
import swal from "sweetalert2";

export default {
  async logIn(authResp) {
    store.dispatch("setIsLoading", true);
    try {
      const user = await (
        await axios.get("/login", {
          headers: { Authorization: `Bearer ${authResp.id_token}` }
        })
      ).data;

      user.expires_at = authResp.expires_at;
      await store.dispatch("setToken", authResp);
      await store.dispatch("setAuthUser", user);
      axios.defaults.headers.common["Authorization"] =
        "Bearer " + store.getters.getToken.id_token;
    } catch (e) {
      if (e.response) {
        if (e.response.status === 404) {
          swal.fire({
            icon: "error",
            title: "User does not exist"
          });
        }
      } else {
        swal.fire({
          icon: "error",
          title: `Oops...`,
          text: "An error occured"
        });
      }
    }
    store.dispatch("setIsLoading", false);
  },
  async getTokenInfo(token) {
    return await axios.get(
      `https://oauth2.googleapis.com/tokeninfo?id_token=${token}`
    );
  },
  registerTrainer(user) {
    return axios.post("/trainers", user);
  },
  registerClient(user) {
    return axios.post("/clients", user);
  },
  async codeIsActive(code) {
    try {
      const resp = await (await axios.get(`/mail/registration/${code}`)).data;
      return resp;
    } catch (e) {
      return false;
    }
  },
  sendInviteLink(email, trainerId) {
    return axios.get("/mail/registration/client", {
      params: {
        trainerId,
        email
      }
    });
  },
  async isValidZipCode(code) {
    try {
      const res = await axios.get(`https://webapi.no/api/v1/zipcode/${code}`);
      if (res.status === 200) {
        if (res.data.data.city) {
          return true;
        }
        return false;
      } else {
        return false;
      }
    } catch (e) {
      console.log(e);
      return false;
    }
  }
};
