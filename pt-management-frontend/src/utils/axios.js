/* eslint-disable no-console */
import axios from "axios";
// import store from './../store';

export const instance = axios.create({
  baseURL: "https://ptmanagementapi20210405122623.azurewebsites.net/api/v1"
  //baseURL: "https://localhost:5001/api/v1"
});

axios.interceptors.request.use(config => {
  // if (store.state.token) {
  //   axios.defaults.headers.common['Authorization'] =
  //     'Bearer ' + store.state.token;
  // }
  return config;
});

axios.interceptors.response.use(res => {
  console.log("Response", res);
  return res;
});

export default instance;
