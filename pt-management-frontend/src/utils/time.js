export default{
    //"Converts" the date and time into ISO Format, but with local time and not UTC
    getLocalISO(date, time){
        return date + "T" + time + ":00Z"
    },
}