import Vue from "vue";
import Vuex from "vuex";
import router from "./../router";
import axios from "./../utils/axios";
import auth from "../controllers/auth.controller";
import swal from "sweetalert2";
import JsonCSV from "vue-json-csv";
Vue.component("downloadCsv", JsonCSV);

Vue.use(JsonCSV);
Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    id_token: null,
    authUser: null,
    isLoading: false
  },
  mutations: {
    SET_TOKEN(state, token) {
      state.id_token = token;
      localStorage.setItem("token", JSON.stringify(token));
    },
    SET_AUTH_USER(state, user) {
      state.authUser = user;
    },
    LOG_IN(state, token, user) {
      state.token = token;
      state.authUser = user;
      localStorage.setItem("token", token);
      router.push({ path: "/" });
    },
    LOG_OUT(state) {
      state.id_token = null;
      state.authUser = null;
      localStorage.removeItem("token");
      axios.defaults.headers.common["Authorization"] = "";
      router.push({ path: "/login" });
    },
    SET_IS_LOADING(state, loading) {
      state.isLoading = loading;
    }
  },
  actions: {
    async setToken({ commit }, token) {
      commit("SET_TOKEN", token);
    },
    async setAuthUser({ commit }, user) {
      commit("SET_AUTH_USER", user);
    },
    logOut({ commit }) {
      commit("LOG_OUT");
    },
    async resumeSession({ state }) {
      state.id_token = JSON.parse(localStorage.getItem("token"));
      if (state.id_token.expires_at < new Date()) {
        this.dispatch("logOut");
        swal.fire({
          icon: "info",
          title: "Session expired!",
          text: "Please log in again"
        });
        return;
      }
      try {
        await auth.logIn(state.id_token);
      } catch (e) {
        console.log(e);
        this.dispatch("logOut");
      }
    },
    setIsLoading({ commit }, loading) {
      commit("SET_IS_LOADING", loading);
    }
  },
  getters: {
    getToken(state) {
      return state.id_token;
    },
    getAuthUser(state) {
      return state.authUser;
    },
    getIsLoading(state) {
      return state.isLoading;
    }
  }
});
