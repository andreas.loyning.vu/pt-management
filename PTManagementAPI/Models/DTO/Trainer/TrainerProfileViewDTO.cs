﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PTManagementAPI.Models.Enum;

namespace PTManagementAPI.Models.DTO.Trainer
{
    public class TrainerProfileViewDTO
    {
        public int TrainerId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public DateTime DOB { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string PostalCode { get; set; }
        public string Image { get; set; }
        public string AdditionalInfo { get; set; }
        public bool IsActive { get; set; }
        public string Role { get; set; }
        public DateTime CreatedAt { get; set; }
        public string ActivationStatus { get; set; }
        public ICollection<int> ClientIds { get; set; }
        public ICollection<int> InvoiceIds { get; set; }
        public ICollection<int> BookingIds { get; set; }
    }
}
