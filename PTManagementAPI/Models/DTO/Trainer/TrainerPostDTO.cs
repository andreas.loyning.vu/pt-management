﻿using PTManagementAPI.Models.DTO.UserProfile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PTManagementAPI.Models.DTO.Trainer
{
    public class TrainerPostDTO
    {
        public string FirstName { get; set; }
        public string Subject { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public DateTime DOB { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string PostalCode { get; set; }
        public string Image { get; set; }
        public string AdditionalInfo { get; set; }
    }
}
