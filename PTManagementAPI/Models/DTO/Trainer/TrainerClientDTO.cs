﻿using System;

namespace PTManagementAPI.Models.DTO.Trainer
{
    public class TrainerClientDTO
    {
        public int ClientId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public DateTime DOB { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string PostalCode { get; set; }
        public string Phone { get; set; }
        public string Image { get; set; }
        public string AdditionalInfo { get; set; }
        public int Sessions { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
