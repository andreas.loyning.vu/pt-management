﻿using PTManagementAPI.Models.Domain;
using PTManagementAPI.Models.DTO.UserProfile;
using PTManagementAPI.Models.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PTManagementAPI.Models.DTO.Trainer
{
    public class TrainerProfileDTO
    {
        [Required]
        public int TrainerId { get; set; }

        public UserDTO User { get; set; }
        public string ActivationStatus { get; set; }
        public ICollection<int> ClientIds { get; set; }
        public ICollection<int> BookingIds { get; set; }
        public ICollection<int> InvoiceIds { get; set; }

    }
}
