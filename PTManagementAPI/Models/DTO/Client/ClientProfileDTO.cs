﻿using System;
using PTManagementAPI.Models.Enum;

namespace PTManagementAPI.Models.DTO
{
    public class ClientProfileDTO
    {
        public int ClientId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public DateTime DOB { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public int TrainerId { get; set; }
        public string TrainerFirstName { get; set; }
        public string TrainerMiddleName { get; set; }
        public string TrainerLastName { get; set; }
        public string Image { get; set; }
        public string AdditionalInfo { get; set; }
        public int Height { get; set; }
        public string PostalCode { get; set; }
        public string Phone { get; set; }
        public WeightGetDTO Weight { get; set; }
        public string Role { get; set; }
        public DateTime CreatedAt { get; set; }
        public int Sessions { get; set; }
    }
}
