﻿using PTManagementAPI.Models.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PTManagementAPI.Models.DTO.Client
{
    public class ClientUpdateDTO
    {
        public int ClientId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public DateTime DOB { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string PostalCode { get; set; }
        public string Image { get; set; }
        public string AdditionalInfo { get; set; }
        public UserRole Role { get; set; }
        public bool IsActive { get; set; }
        public int TrainerId { get; set; }
        public int Height { get; set; }
    }
}
