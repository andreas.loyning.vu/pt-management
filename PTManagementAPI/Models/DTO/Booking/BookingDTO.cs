﻿using PTManagementAPI.Models.Enum;
using System;
using System.ComponentModel.DataAnnotations;

namespace PTManagementAPI.Models.DTO
{
    public class BookingDTO
    {
        public int BookingId { get; set; }

        [Required]
        public int TrainerId { get; set; }

        [Required]
        public int ClientId { get; set; }

        [Required]
        public int VenueId { get; set; }

        [Required]
        public DateTime BookingDate { get; set; }

        [Required]
        public DateTime SessionDate { get; set; }

        [Required]
        [Range(0, 2)]
        public BookingStatus Status { get; set; }

        //Required on Description?
        public string Description { get; set; }

    }
}