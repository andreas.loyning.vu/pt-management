﻿using PTManagementAPI.Models.Enum;
using System;
using System.ComponentModel.DataAnnotations;

namespace PTManagementAPI.Models.DTO.Booking
{
    public class BookingStatusDTO
    {
        [Required]
        [Range(0, 2)]
        public BookingStatus Status { get; set; }
    }
}