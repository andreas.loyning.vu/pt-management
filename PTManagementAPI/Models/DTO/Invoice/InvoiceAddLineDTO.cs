using PTManagementAPI.Models.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PTManagementAPI.Models.DTO
{
    public class InvoiceaddLineDTO
    {
        public ICollection<int> InvoiceLineIds { get; set; }
    }
}