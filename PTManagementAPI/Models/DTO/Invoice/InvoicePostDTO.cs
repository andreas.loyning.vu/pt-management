using PTManagementAPI.Models.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PTManagementAPI.Models.DTO
{
    /// <summary>
    /// Invoice DTO for posting a new invoice
    /// </summary>
    public class InvoicePostDTO
    {
        public int ClientId { get; set; }

        public int TrainerId { get; set; }

        public DateTime IssueDate { get; set; }

        public DateTime DueDate { get; set; }

        [Range(0,3)]
        public InvoiceStatus Status { get; set; }
    }
}