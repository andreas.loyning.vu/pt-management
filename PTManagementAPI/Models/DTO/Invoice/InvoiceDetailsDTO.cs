using PTManagementAPI.Models.Enum;
using System;
using System.Collections.Generic;

namespace PTManagementAPI.Models.DTO
{
    /// <summary>
    /// Details DTO for invoice, displays details about each invoice line in the invoice
    /// </summary>
    public class InvoiceDetailsDTO
    {
        public int InvoiceId { get; set; }

        public int ClientId { get; set; }

        public string ClientName { get; set; }

        public int TrainerId { get; set; }

        public string TrainerName { get; set; }

        public DateTime IssueDate { get; set; }

        public DateTime DueDate { get; set; }

        public InvoiceStatus Status { get; set; }

        public ICollection<InvoiceLineDTO> InvoiceLines { get; set; }
    }
}