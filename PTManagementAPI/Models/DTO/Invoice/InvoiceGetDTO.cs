using PTManagementAPI.Models.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PTManagementAPI.Models.DTO
{
    /// <summary>
    /// Invoice DTO for showing minimal info about the containing invoicelines
    /// </summary>
    public class InvoiceGetDTO
    {
        public int InvoiceId { get; set; }

        public int ClientId { get; set; }
        public string ClientName { get; set; }

        public int TrainerId { get; set; }
        public string TrainerName { get; set; }

        public DateTime IssueDate { get; set; }

        public DateTime DueDate { get; set; }

        [Range(0,3)]
        public InvoiceStatus Status { get; set; }

        public ICollection<int> InvoiceLineIds { get; set; }
    }
}
