using PTManagementAPI.Models.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PTManagementAPI.Models.DTO
{
    /// <summary>
    /// DTO for updating the status of an invoice
    /// </summary>
    public class InvoiceStatusDTO
    {   
        [Range(0,3)]
        public InvoiceStatus Status { get; set; }
    }
}