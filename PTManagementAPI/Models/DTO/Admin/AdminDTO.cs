using PTManagementAPI.Models.Domain;
using PTManagementAPI.Models.DTO.UserProfile;
using PTManagementAPI.Models.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PTManagementAPI.Models.DTO
{
    public class AdminDTO
    {
        [Required]
        public int AdminId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Image { get; set; }
        public string Role { get; set; }
        public string Email { get; set; }
    }
}