using System;
using System.ComponentModel.DataAnnotations;

namespace PTManagementAPI.Models.DTO
{
    public class WeightGetDTO
    {
        public int WeightId { get; set; }
        public int ClientId { get; set; }
        public DateTime Date { get; set; }
        public decimal WeightAtTime { get; set; }
    }
}