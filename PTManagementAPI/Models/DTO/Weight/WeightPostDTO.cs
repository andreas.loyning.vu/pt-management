using System;
using System.ComponentModel.DataAnnotations;

namespace PTManagementAPI.Models.DTO
{
    public class WeightPostDTO
    {
        public int ClientId { get; set; }
        public DateTime Date { get; set; }
        public decimal WeightAtTime { get; set; }
    }
}