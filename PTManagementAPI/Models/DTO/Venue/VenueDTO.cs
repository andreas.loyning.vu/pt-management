﻿using PTManagementAPI.Models.Enum;
using System.ComponentModel.DataAnnotations;

namespace PTManagementAPI.Models.DTO
{
    public class VenueDTO
    {
        public int VenueId { get; set; }

        [Required]
        public string Address { get; set; }

        [Required]
        [RegularExpression(@"\d{4}")]
        public string PostalCode { get; set; }

        [Required]
        [Range(0, 2)]
        public VenueType Type { get; set; }

        [Required]
        public bool IsActive { get; set; }
    }
}