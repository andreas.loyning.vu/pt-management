﻿using PTManagementAPI.Models.Enum;
using System;
using System.ComponentModel.DataAnnotations;

namespace PTManagementAPI.Models.DTO.Venue
{
    public class VenuePostDTO
    {
        [Required]
        public string Address { get; set; }

        [Required]
        [RegularExpression(@"\d{4}")]
        public string PostalCode { get; set; }

        [Required]
        [Range(0, 2)]
        public VenueType Type { get; set; }

        [Required]
        public bool IsActive { get; set; }
    }
}