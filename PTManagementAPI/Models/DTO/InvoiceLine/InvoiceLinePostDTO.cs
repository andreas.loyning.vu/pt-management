﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PTManagementAPI.Models.DTO.InvoiceLine
{
    public class InvoiceLinePostDTO
    {
        [Required]
        public int InvoiceId { get; set; }

        public int Quantity { get; set; }
        public decimal Price { get; set; }

        [StringLength(500)]
        public string Description { get; set; }
    }
}
