namespace PTManagementAPI.Models.Enum
{
    public enum ActivationStatus
    {
        Pending,
        Declined,
        Accepted
    }
}