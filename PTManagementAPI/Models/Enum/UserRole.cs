﻿namespace PTManagementAPI.Models.Enum
{
    public enum UserRole
    {
        Admin,
        Trainer,
        Client
    }
}