﻿namespace PTManagementAPI.Models.Enum
{
    /// <summary>
    /// 0 - Booked
    /// 1 - Completed
    /// 2 - Cancelled
    /// </summary>
    public enum BookingStatus
    {
        Booked,
        Completed,
        Cancelled
    }
}