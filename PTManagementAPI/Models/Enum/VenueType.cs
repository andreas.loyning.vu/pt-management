﻿namespace PTManagementAPI.Models.Enum
{
    /// <summary>
    /// 0 - Gym
    /// 1 - Public
    /// 2 - Home
    /// </summary>
    public enum VenueType
    {
        Gym,
        Public,
        Home
    }
}