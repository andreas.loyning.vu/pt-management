﻿namespace PTManagementAPI.Models.Enum
{
    /// <summary>
    /// Enums for different invoicestatuses
    /// Sent - 0
    /// Payed - 1
    /// Cancelled - 2
    /// Expired - 3
    /// </summary>
    public enum InvoiceStatus
    {
        Sent,
        Payed,
        Cancelled,
        Expired
    }
}