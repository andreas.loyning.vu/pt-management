﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PTManagementAPI.Models.Domain
{
    public class City
    {
        //Non auto increment
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Required]
        [RegularExpression(@"\d{4}")]
        public string PostalCode { get; set; }

        public string CityName { get; set; }
    }
}