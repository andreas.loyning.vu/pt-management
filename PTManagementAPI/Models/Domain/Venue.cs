﻿using PTManagementAPI.Models.Enum;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PTManagementAPI.Models.Domain
{
    public class Venue
    {
        [Key]
        public int VenueId { get; set; }

        [StringLength(50)]
        public string Address { get; set; }

        //FK for postal code
        [Required]
        [ForeignKey("City")]
        [RegularExpression(@"\d{4}")]
        public string PostalCode { get; set; }

        //Not sure about this part yet
        public virtual City City { get; set; }

        [Required]
        public VenueType Type { get; set; }

        [Required]
        public bool IsActive { get; set; }

        public virtual ICollection<Booking>? Bookings { get; set; }
    }
}