﻿using PTManagementAPI.Models.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PTManagementAPI.Models.Domain
{
    public class Invoice
    {
        [Key]
        public int InvoiceId { get; set; }

        public int ClientId { get; set; }
        public virtual Client Client { get; set; }

        [Required]
        public int TrainerId { get; set; }

        public virtual Trainer Trainer { get; set; }

        [Required]
        public DateTime IssueDate { get; set; }

        [Required]
        public DateTime DueDate { get; set; }

        [Required]
        public InvoiceStatus Status { get; set; }

        [Required]
        public virtual ICollection<InvoiceLine> InvoiceLines { get; set; }
    }
}