﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PTManagementAPI.Models.Domain
{
    public class Client
    {
        [Key]
        public int ClientId { get; set; }

        [Required]
        public int UserId { get; set; }

        public virtual User User { get; set; }

        [Required]
        public int TrainerId { get; set; }
        public virtual Trainer Trainer { get; set; }
        public int? Height { get; set; }
        public DateTime? LastSession { get; set; }
        public int Sessions { get; set; } = 0;
        public virtual ICollection<Weight> WeightHistory { get; set; }
        public virtual ICollection<Invoice>? Invoices { get; set; }
        public virtual ICollection<Booking>? Bookings { get; set; }
    }
}