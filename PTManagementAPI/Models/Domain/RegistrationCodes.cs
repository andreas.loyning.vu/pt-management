﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PTManagementAPI.Models.Domain
{
    public class RegistrationCodes
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }
        [Required]
        public int TrainerId { get; set; }
        [Required]
        public DateTime IssueDate { get; set; }

        [Required]
        public bool isActive { get; set; }
    }
}
