﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using PTManagementAPI.Models.Enum;

namespace PTManagementAPI.Models.Domain
{
    public class Trainer
    {
        [Key]
        public int TrainerId { get; set; }

        [Required]
        public int UserId { get; set; }
        public virtual User User { get; set; }
        public ActivationStatus ActivationStatus { get; set; }
        public virtual ICollection<Client>? Clients { get; set; }
        public virtual ICollection<Invoice>? Invoices { get; set; }
        public virtual ICollection<Booking>? Bookings { get; set; }
    }
}