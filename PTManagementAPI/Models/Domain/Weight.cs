﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PTManagementAPI.Models.Domain
{
    public class Weight
    {
        [Key]
        public int WeightId { get; set; }

        [Required]
        public int ClientId { get; set; }

        public DateTime Date { get; set; }
        
        public decimal WeightAtTime { get; set; }
    }
}