using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PTManagementAPI.Models.Domain
{
    public class Admin
    {
        [Key]
        public int AdminId { get; set; }

        [Required]
        public int UserId { get; set; }

        public virtual User User { get; set; }
    }
}