﻿using Microsoft.EntityFrameworkCore;
using PTManagementAPI.Models.Enum;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PTManagementAPI.Models.Domain
{
    public class User
    {
        [Key]
        public int UserId { get; set; }

        [Required]
        public string Subject { get; set; }

        [Required]
        [StringLength(50)]
        public string FirstName { get; set; }

        [StringLength(50)]
        public string MiddleName { get; set; }

        [Required]
        [StringLength(50)]
        public string LastName { get; set; }

        [Required]
        public DateTime DOB { get; set; }

        [Required]
        [StringLength(255)]
        public string Email { get; set; }

        [Required]
        [StringLength(30)]
        public string Phone { get; set; }

        [Required]
        [StringLength(50)]
        public string Address { get; set; }

        [StringLength(255)]
        public string Image { get; set; }

        [Required]
        [RegularExpression(@"\d{4}")]
        [ForeignKey("City")]
        public string PostalCode { get; set; }

        //Not sure about this part yet
        public virtual City City { get; set; }

        [StringLength(2000)]
        public string AdditionalInfo { get; set; }

        [Required]
        public UserRole Role { get; set; }

        [Required]
        public bool IsActive { get; set; }

        [Required]
        public DateTime CreatedAt { get; set; } = DateTime.Now;
    }
}