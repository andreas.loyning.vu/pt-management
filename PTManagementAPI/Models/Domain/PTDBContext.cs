﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PTManagementAPI.Models.Domain
{
    public class PTDBContext : DbContext
    {
        public PTDBContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder
                .UseLazyLoadingProxies()
                .EnableSensitiveDataLogging();
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            
            //Setting precision to the decimal values
            builder.Entity<Weight>().Property(w => w.WeightAtTime).HasPrecision(5, 2);
            builder.Entity<InvoiceLine>().Property(i => i.Price).HasPrecision(10, 2);
            // Set the subject field to be unique
            builder.Entity<User>().HasIndex(u => u.Subject).IsUnique();
            var cascadeFKs = builder.Model.GetEntityTypes()
                .SelectMany(t => t.GetForeignKeys())
                .Where(fk => !fk.IsOwnership && fk.DeleteBehavior == DeleteBehavior.Cascade);

            foreach (var fk in cascadeFKs)
                fk.DeleteBehavior = DeleteBehavior.Restrict;

            var bookingList = new List<Booking>();
            var clientList = new List<Client>();
            var invoiceList = new List<InvoiceLine>();

            var venue = new Venue { VenueId = 1, Address = "oslo 1", PostalCode = "0001", Type = Enum.VenueType.Gym, IsActive = true };
            var client = new Client { ClientId = 1, UserId = 1, TrainerId = 1, Height = 250 };
            clientList.Add(client);

            var adminUser = new User { UserId =  3, FirstName = "Admin", LastName = "Admin", DOB = DateTime.Now, Email = "pt.management.noreply@gmail.com", Phone = "123123123", Address = "PT Management", IsActive = true, PostalCode = "0001", Subject = "102592725094451221497", Role = Enum.UserRole.Admin};
            var user1 = new User { UserId = 1, FirstName = "Andreas", LastName = "T", DOB = DateTime.Now, Email = "test", Phone = "129387", Address = "lkajsdfasdf", IsActive = false, PostalCode = "0001", Subject = "" };
            var user2 = new User { UserId = 2, FirstName = "Muneeb", LastName = "Rana", DOB = DateTime.Now, Email = "test", Phone = "129387", Address = "lkajsdfasdf", IsActive = false, PostalCode = "0001", Subject = "asd" };
            var admin = new Admin { AdminId = 1, UserId = 3 };
            var trainer = new Trainer { UserId = 2, TrainerId = 1 };
            var booking = new Booking { BookingId = 1, TrainerId = 1, ClientId = 1, VenueId = 1, BookingDate = DateTime.Now, SessionDate = DateTime.Now, Status = Enum.BookingStatus.Cancelled, Description = "First Booking", Client = client, Trainer = trainer, Venue = venue };
            bookingList.Add(booking);

            var city = new City { PostalCode = "0001", CityName = "Oslo" };
            var weight = new Weight { WeightId = 1, ClientId = 1, Date = DateTime.Now, WeightAtTime = 99 };
            var invoiceLine = new InvoiceLine
            {
                InvoiceLineId = 1,
                InvoiceId = 1,
                Quantity = 5,
                Price = 1337.25m,
                Description = "This is very expensive"
            };
            invoiceList.Add(invoiceLine);

            var invoice = new Invoice
            {
                InvoiceId = 1,
                ClientId = 1,
                TrainerId = 1,
                IssueDate = DateTime.Now,
                DueDate = DateTime.Now,
                Status = Enum.InvoiceStatus.Sent
            };

            var regcodes = new RegistrationCodes { Id = new Guid("00000000-0000-0000-0000-000000000001"), TrainerId = 1, isActive = true };
            builder.Entity<User>().HasData(user1, user2, adminUser);
            //builder.Entity<Booking>().HasData(booking);
            builder.Entity<Client>().HasData(client);
            builder.Entity<Trainer>().HasData(trainer);
            builder.Entity<InvoiceLine>().HasData(invoiceLine);
            builder.Entity<Invoice>().HasData(invoice);
            builder.Entity<City>().HasData(city);
            builder.Entity<Venue>().HasData(venue);
            builder.Entity<Weight>().HasData(weight);
            builder.Entity<RegistrationCodes>().HasData(regcodes);
            builder.Entity<Admin>().HasData(admin);
        }

        public DbSet<Booking> Bookings { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<Invoice> Invoices { get; set; }
        public DbSet<InvoiceLine> InvoiceLines { get; set; }
        public DbSet<Trainer> Trainers { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Venue> Venues { get; set; }
        public DbSet<Weight> Weights { get; set; }
        public DbSet<Admin> Admins { get; set; }
        public DbSet<RegistrationCodes> RegistrationCodes { get; set; }
    }
}