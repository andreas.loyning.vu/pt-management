﻿using PTManagementAPI.Models.Enum;
using System;
using System.ComponentModel.DataAnnotations;

namespace PTManagementAPI.Models.Domain
{
    public class Booking
    {
        [Key]
        public int BookingId { get; set; }

        [Required]
        public int TrainerId { get; set; }

        public virtual Trainer Trainer { get; set; }

        [Required]
        public int ClientId { get; set; }

        public virtual Client Client { get; set; }

        [Required]
        public int VenueId { get; set; }

        public virtual Venue Venue { get; set; }

        [Required]
        public DateTime BookingDate { get; set; }

        [Required]
        public DateTime SessionDate  { get; set; }

        [Required]
        public BookingStatus Status { get; set; }

        //Required on Description?
        public string Description { get; set; }
    }
}