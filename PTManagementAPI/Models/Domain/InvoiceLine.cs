﻿using System.ComponentModel.DataAnnotations;

namespace PTManagementAPI.Models.Domain
{
    public class InvoiceLine
    {
        [Key]
        public int InvoiceLineId { get; set; }

        [Required]
        public int InvoiceId { get; set; }

        public virtual Invoice Invoice { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }

        [StringLength(500)]
        public string Description { get; set; }
    }
}