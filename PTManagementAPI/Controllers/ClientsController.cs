using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PTManagementAPI.Models.Domain;
using PTManagementAPI.Models.DTO;
using PTManagementAPI.Models.DTO.Client;
using PTManagementAPI.Models.Enum;
using PTManagementAPI.Services;

namespace PTManagementAPI.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class ClientsController : ControllerBase
    {
        private readonly ClientService _service;
        private readonly IMapper _mapper;

        public ClientsController(ClientService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        /// <summary>
        /// Fetches all clients
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize("Admin")]
        public async Task<ActionResult<IEnumerable<ClientProfileDTO>>> GetClients()
        {

            ICollection<ClientProfileDTO> list = _mapper.Map<ICollection<ClientProfileDTO>>(await _service.GetClients());

            if (list.Count > 0) return Ok(list);

            return NoContent();
        }

        /// <summary>
        /// Fetches a client based on the provided Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [Authorize("Weight")]
        public async Task<ActionResult<ClientProfileDTO>> GetClient(int id)
        {
            var clientDto = _mapper.Map<ClientProfileDTO>(await _service.GetClient(id));

            if (clientDto == null)
            {
                return NotFound();
            }

            return clientDto;
        }

        /// <summary>
        /// Updates a client based on provided Id and updated information
        /// </summary>
        /// <param name="id"></param>
        /// <param name="clientDTO"></param>
        /// <returns></returns>
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        [Authorize("Client")]

        public async Task<IActionResult> PutClient(int id, ClientUpdateDTO clientDTO)
        {
            if (id != clientDTO.ClientId)
            {
                return BadRequest();
            }

            User user = _mapper.Map<User>(clientDTO);

            await _service.UpdateClientUser(id, user);

            Client client = await _service.GetClient(id);
            _mapper.Map(clientDTO, client);
            await _service.UpdateClient(id, client);

            return NoContent();
        }

        /// <summary>
        /// Adds a client to the database
        /// </summary>
        /// <param name="clientPostDTO"></param>
        /// <returns></returns>
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<ClientProfileDTO>> PostClient(ClientPostDTO clientPostDTO)
        {
            User user = _mapper.Map<User>(clientPostDTO);
            Client client = _mapper.Map<Client>(clientPostDTO);
            client.User = user;
            client.User.IsActive = false;
            client.User.Role = UserRole.Client;

            RegistrationCodes guid = await _service.GetGuid(clientPostDTO.Guid);
            if (guid is not null && guid.isActive)
            {
                client.TrainerId = guid.TrainerId;
                
                Client clientDM = await _service.PostClient(client);
                if (clientDM is null)
                {
                    return BadRequest();
                }

                _service.DeactivateGuid(clientPostDTO.Guid);

                clientPostDTO = _mapper.Map<ClientPostDTO>(clientDM.User);
                _mapper.Map(clientDM, clientPostDTO);

                return CreatedAtAction(nameof(GetClient), new { id = clientDM.ClientId }, clientPostDTO);
            }

            return BadRequest();
        }
    }
}
