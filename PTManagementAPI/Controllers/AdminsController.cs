using System;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using PTManagementAPI.Models.Domain;
using PTManagementAPI.Models.DTO;
using PTManagementAPI.Models.Enum;
using PTManagementAPI.Services;

namespace PTManagementAPI.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class AdminsController : ControllerBase
    {
        private readonly UserService _service;
        private readonly HttpContextAccessor _httpContext;
        private readonly IMapper _mapper;

        public AdminsController(HttpContextAccessor httpContext, UserService userService, IMapper mapper)
        {
            _service = userService;
            _mapper = mapper;
            _httpContext = httpContext;
        }

        [HttpGet]
        public async Task<ActionResult<AdminDTO>> GetAdmin(int id)
        {
            try
            {
                ClaimsIdentity claims = HttpContext.User.Identity as ClaimsIdentity;
                string subject = claims.FindFirst("sub").Value;
                User user = _service.GetUser(subject);

                if (user is null)
                {
                    return NotFound();
                }

                Admin admin = await _service.GetAdmin(user);
                if (admin is null)
                {
                    return NotFound();
                }
                
                AdminDTO adminDTO = _mapper.Map<AdminDTO>(admin);
                return adminDTO;
            }
            catch (Exception e)
            {
                return Unauthorized();
            }
        }
    }
}