using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PTManagementAPI.Models.Domain;
using PTManagementAPI.Models.DTO;
using PTManagementAPI.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PTManagementAPI.Controllers
{
    [Route("api/v1/[controller]")]
    [Authorize("Weight")]
    [ApiController]
    public class WeightsController : ControllerBase
    {
        private readonly WeightService _service;
        private readonly IMapper _mapper;

        public WeightsController(WeightService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        /// <summary>
        /// Fetches all weights
        /// </summary>
        /// <returns>List of weights</returns>
        [HttpGet]
        [Authorize("Client")]
        public async Task<ActionResult<IEnumerable<WeightGetDTO>>> GetWeights()
        {
            ICollection<Weight> weightsDM = await _service.GetWeights();
            ICollection<WeightGetDTO> weightsDTO = _mapper.Map<ICollection<WeightGetDTO>>(weightsDM);

            if (weightsDTO.Count > 0)
            {
                return Ok(weightsDTO);
            }

            return NoContent();
        }

        /// <summary>
        /// Fetches an weight with the specified Id
        /// </summary>
        /// <param name="id">Id of the invoice</param>
        /// <returns>Invoice</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<WeightGetDTO>> GetWeight(int id)
        {
            Weight weightDM = await _service.GetWeight(id);
            WeightGetDTO weightDTO = _mapper.Map<WeightGetDTO>(weightDM);

            if (weightDTO is null)
            {
                return NotFound();
            }

            return weightDTO;
        }

        /// <summary>
        /// Updates the specified weight
        /// </summary>
        /// <param name="id">Id of the weight</param>
        /// <param name="weight">Updated weight</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [Authorize("Client")]
        public async Task<IActionResult> PutWeight(int id, WeightGetDTO weight)
        {
            if (id != weight.WeightId)
            {
                return BadRequest();
            }

            Weight weightDM = _mapper.Map<Weight>(weight);

            bool success = await _service.UpdateWeight(id, weightDM);
            if (!success)
            {
                return NotFound();
            }

            return NoContent();
        }

        /// <summary>
        /// Adds a new weight
        /// </summary>
        /// <param name="weight">Weight to be added</param>
        /// <returns>Weight</returns>
        [HttpPost]
        [Authorize("Client")]
        public async Task<ActionResult<WeightGetDTO>> PostWeight(WeightPostDTO weight)
        {
            Weight weightDM = _mapper.Map<Weight>(weight);

            weightDM = await _service.PostWeight(weightDM);
            if (weightDM is null)
            {
                return BadRequest();
            }
            WeightGetDTO weightDTO = _mapper.Map<WeightGetDTO>(weightDM);

            return CreatedAtAction(nameof(GetWeight), new { id = weightDM.WeightId }, weightDTO);
        }

        /// <summary>
        /// Deletes the specified weight
        /// </summary>
        /// <param name="id">Id of the weight</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteWeight(int id)
        {
            bool success = await _service.DeleteWeight(id);
            if (!success)
            {
                return NotFound();
            }

            return NoContent();
        }

        /// <summary>
        /// Gets all the weights from a client
        /// </summary>
        /// <param name="id">Id of the client</param>
        /// <returns></returns>
        [HttpGet("client/{id}")]
        public async Task<ActionResult<ICollection<WeightGetDTO>>> GetClientsWeights(int id)
        {
            ICollection<Weight> weightsDM = await _service.GetClientsWeights(id);
            ICollection<WeightGetDTO> weightsDTO = _mapper.Map<ICollection<WeightGetDTO>>(weightsDM);

            if (weightsDTO.Count > 0)
            {
                return Ok(weightsDTO);
            }

            return NoContent();
        }
    }
}