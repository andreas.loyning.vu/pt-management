﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using PTManagementAPI.Models.Domain;
using PTManagementAPI.Models.Enum;
using PTManagementAPI.Services;

namespace PTManagementAPI.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly HttpContextAccessor _httpContext;
        private readonly UserService _service;
        public LoginController(HttpContextAccessor httpContext, UserService service)
        {
            _service = service;
            _httpContext = httpContext;
        }

        [HttpGet]
        public async Task<ActionResult> Login()
        {
            try
            {
                ClaimsIdentity claims = HttpContext.User.Identity as ClaimsIdentity;
                string subject = claims.FindFirst("sub").Value;
                User user = _service.GetUser(subject);

                if (user is null)
                {
                    return NotFound();
                }

                if (!user.IsActive) {
                    return Unauthorized();
                }
                
                switch (user.Role)
                {
                    case UserRole.Admin:
                        {
                            Admin admin = await _service.GetAdmin(user);
                            return RedirectToAction("getAdmin", new RouteValueDictionary(new { Controller = "Admins", action = "getAdmin", id = admin.AdminId }));
                        }
                    case UserRole.Trainer:
                        {
                            Trainer trainer = await _service.GetTrainer(user);
                            return RedirectToAction("getTrainer", new RouteValueDictionary(new { Controller = "Trainers", action = "getTrainer", id = trainer.TrainerId }));
                        }
                    case UserRole.Client:
                        {
                            Client client = await _service.GetClient(user);
                            return RedirectToAction("getClient", new RouteValueDictionary(new { Controller = "Clients", action = "getClient", id = client.ClientId }));
                        }
                    default: return BadRequest();
                }
            }
            catch (Exception e)
            {
                return Unauthorized();
            }
        }
    }
}
