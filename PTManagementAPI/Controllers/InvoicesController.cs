using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PTManagementAPI.Models.Domain;
using PTManagementAPI.Models.DTO;
using PTManagementAPI.Services;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace PTManagementAPI.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class InvoicesController : ControllerBase
    {
        private readonly InvoiceService _service;
        private readonly ClientService _clientService;
        private readonly TrainerService _trainerService;

        private readonly IMapper _mapper;
        private readonly HttpContextAccessor _context;

        public InvoicesController(InvoiceService service, ClientService clientService, TrainerService trainerService, IMapper mapper, HttpContextAccessor context)
        {
            _service = service;
            _clientService = clientService;
            _trainerService = trainerService;
            _mapper = mapper;
            _context = context;
        }

        /// <summary>
        /// Fetches all invoices
        /// </summary>
        /// <returns>List of invoices</returns>
        [HttpGet]
        [Authorize("Admin")]
        public async Task<ActionResult<IEnumerable<InvoiceGetDTO>>> GetInvoices()
        {
            try
            {
                var test = HttpContext.User.Identity as ClaimsIdentity;
                System.Console.WriteLine(test.FindFirst("sub").Value);
            }
            catch (Exception e)
            {
                System.Console.WriteLine(e);
            }

            ICollection<Invoice> invoicesDM = await _service.GetInvoices();

            if (invoicesDM.Count > 0)
            {
                ICollection<InvoiceGetDTO> invoicesDTO = _mapper.Map<ICollection<InvoiceGetDTO>>(invoicesDM);
                foreach (InvoiceGetDTO invoice in invoicesDTO)
                {
                    Client client = await _clientService.GetClient(invoice.ClientId);
                    Trainer trainer = await _trainerService.GetTrainer(invoice.TrainerId);
                    invoice.ClientName = client.User.FirstName + " " + client.User.LastName;
                    invoice.TrainerName = trainer.User.FirstName + " " + trainer.User.LastName;
                }
                return Ok(invoicesDTO);
            }

            return NoContent();
        }

        /// <summary>
        /// Fetches all invoices with invoiceLines
        /// </summary>
        /// <returns>List of invoices</returns>
        [HttpGet("details")]
        [Authorize("Admin")]
        public async Task<ActionResult<IEnumerable<InvoiceDetailsDTO>>> GetInvoicesDetail()
        {
            try
            {
                var test = HttpContext.User.Identity as ClaimsIdentity;
                System.Console.WriteLine(test.FindFirst("sub").Value);
            }
            catch (Exception e)
            {
                System.Console.WriteLine(e);
            }

            ICollection<Invoice> invoicesDM = await _service.GetInvoices();

            if (invoicesDM.Count > 0)
            {
                ICollection<InvoiceDetailsDTO> invoicesDTO = _mapper.Map<ICollection<InvoiceDetailsDTO>>(invoicesDM);
                foreach (InvoiceDetailsDTO invoice in invoicesDTO)
                {
                    Client client = await _clientService.GetClient(invoice.ClientId);
                    Trainer trainer = await _trainerService.GetTrainer(invoice.TrainerId);
                    invoice.ClientName = client.User.FirstName + " " + client.User.LastName;
                    invoice.TrainerName = trainer.User.FirstName + " " + trainer.User.LastName;
                }
                return Ok(invoicesDTO);
            }

            return NoContent();
        }

        /// <summary>
        /// Fetches an invoice with the specified Id
        /// </summary>
        /// <param name="id">Id of the invoice</param>
        /// <returns>Invoice</returns>
        [HttpGet("{id}")]
        [Authorize("Trainer")]
        public async Task<ActionResult<InvoiceDetailsDTO>> GetInvoice(int id)
        {
            Invoice invoice = await _service.GetInvoice(id);

            if (invoice is null)
            {
                return NotFound();
            }

            return _mapper.Map<InvoiceDetailsDTO>(invoice);
        }

        /// <summary>
        /// Updates the specified Invoice
        /// </summary>
        /// <param name="id">Id of the invoice</param>
        /// <param name="invoice">Updated invoice</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [Authorize("Trainer")]
        public async Task<IActionResult> PutInvoice(int id, InvoiceGetDTO invoice)
        {
            if (id != invoice.InvoiceId)
            {
                return BadRequest();
            }
            Invoice invoiceDM = _mapper.Map<Invoice>(invoice);

            bool success = await _service.UpdateInvoice(id, invoiceDM);
            if (!success)
            {
                return NotFound();
            }

            return NoContent();
        }

        /// <summary>
        /// Adds a new invoice
        /// </summary>
        /// <param name="invoice">invoice to be added</param>
        /// <returns>Invoice</returns>
        [HttpPost]
        [Authorize("Trainer")]
        public async Task<ActionResult<InvoiceDetailsDTO>> PostInvoice(InvoicePostDTO invoice)
        {
            Invoice invoiceDM = _mapper.Map<Invoice>(invoice);
            invoiceDM = await _service.PostInvoice(invoiceDM);
            if (invoiceDM is null)
            {
                return BadRequest();
            }

            InvoiceDetailsDTO invoiceDTO = _mapper.Map<InvoiceDetailsDTO>(invoiceDM);
            return CreatedAtAction("GetInvoice", new { id = invoiceDM.InvoiceId }, invoiceDTO);
        }

        /// <summary>
        /// Deletes the specified invoice
        /// </summary>
        /// <param name="id">Id of the invoice</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [Authorize("Admin")]
        public async Task<IActionResult> DeleteInvoice(int id)
        {
            bool success = await _service.DeleteInvoice(id);
            if (!success)
            {
                return NotFound();
            }

            return NoContent();
        }

        /// <summary>
        /// Updates the status of the specified invoice
        /// </summary>
        /// <param name="id">id of the invoice</param>
        /// <param name="status">status</param>
        /// <returns></returns>
        [HttpPatch("{id}/status")]
        [Authorize("Trainer")]
        public async Task<ActionResult> UpdateStatus(int id, InvoiceStatusDTO status)
        {
            bool success = await _service.SetInvoiceStatus(id, status.Status);
            if (!success)
            {
                return NotFound();
            }

            return NoContent();
        }

        [HttpPost("{id}/invoicelines")]
        [Authorize("Trainer")]
        public async Task<ActionResult> AddInvoiceLine(int id, InvoiceaddLineDTO invoiceLines)
        {
            bool success = await _service.AddInvoiceLineToInvoice(id, invoiceLines.InvoiceLineIds);
            if (!success)
            {
                return BadRequest();
            }

            return NoContent();
        }

        /// <summary>
        /// Downloads invoice
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}/pdf")]
        [Authorize("Trainer")]
        public async Task<ActionResult> DownloadInvoice(int id)
        {
            Invoice invoice = await _service.GetInvoice(id);

            // Generate pdf file
            byte[] invoicePdf = await _service.GeneratePdf(id);

            if (invoicePdf is null)
            {
                return NotFound();
            }

            return File(invoicePdf, "application/octet-stream", "invoice.pdf");
        }

        /// <summary>
        /// Getting all invoice lines for an invoice
        /// </summary>
        [HttpGet("{id}/invoicelines")]
        [Authorize("Trainer")]
        public async Task<ActionResult<ICollection<InvoiceLineDTO>>> GetInvoiceLines(int id)
        {
            ICollection<InvoiceLine> list = await _service.GetInvoiceLinesForInvoice(id);
            if (list is null)
            {
                return BadRequest();
            }
            return Ok(_mapper.Map<ICollection<InvoiceLineDTO>>(list));
        }
    }
}