﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PTManagementAPI.Models.Domain;
using PTManagementAPI.Models.DTO;
using PTManagementAPI.Models.DTO.Booking;
using PTManagementAPI.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PTManagementAPI.Controllers
{
    [Authorize("Trainer")]
    [Route("api/v1/[controller]")]
    [ApiController]
    public class BookingsController : ControllerBase
    {
        private readonly BookingService _service;
        private readonly IMapper _mapper;

        public BookingsController(BookingService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        /// <summary>
        /// Fetches all Bookings
        /// </summary>
        /// <returns>IEnumerable of type BookingDTO</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<BookingDTO>>> GetBookings()
        {
            ICollection<BookingDTO> list = _mapper.Map<ICollection<BookingDTO>>(await _service.GetBookings());
            if (list.Count > 0)
            {
                return Ok(list);
            }
            return NoContent();
        }

        /// <summary>
        /// Fetches booking specified by Booking Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<BookingDTO>> GetBooking(int id)
        {
            BookingDTO booking = _mapper.Map<BookingDTO>(await _service.GetBooking(id));
            if (booking is null)
            {
                return NotFound();
            }
            return booking;
        }

        /// <summary>
        /// Fetches all bookings for a client
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("client/{id}")]

        public async Task<ActionResult<IEnumerable<BookingDTO>>> GetClientBookings(int id)
        {
            ICollection<BookingDTO> clientsBookings = _mapper.Map<ICollection<BookingDTO>>(await _service.GetClientsBookings(id));
            if (clientsBookings.Count > 0)
            {
                return Ok(clientsBookings);
            }
            return NoContent();
        }

        /// <summary>
        /// Fetches all bookings for a trainer
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("trainer/{id}")]

        public async Task<ActionResult<IEnumerable<BookingDTO>>> GetTrainerBookings(int id)
        {
            ICollection<BookingDTO> trainersBookings = _mapper.Map<ICollection<BookingDTO>>(await _service.GetTrainersBookings(id));
            if (trainersBookings.Count > 0)
            {
                return Ok(trainersBookings);
            }
            return NoContent();
        }

        /// <summary>
        /// Updates a booking with the specified Id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="booking"></param>
        /// <returns></returns>
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBooking(int id, BookingDTO booking)
        {
            if (id != booking.BookingId)
            {
                return BadRequest();
            }
            bool success = await _service.UpdateBooking(id, _mapper.Map<Booking>(booking));
            if (!success)
            {
                return BadRequest();
            }
            return NoContent();
        }

        /// <summary>
        /// Adds a new booking to  the database
        /// </summary>
        /// <param name="booking"></param>
        /// <returns></returns>
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<BookingDTO>> PostBooking(BookingPostDTO booking)
        {
            BookingDTO bookingDTO = _mapper.Map<BookingDTO>(await _service.PostBooking(_mapper.Map<Booking>(booking)));
            if (bookingDTO is not null)
            {
                return CreatedAtAction(nameof(GetBooking), new { id = bookingDTO.BookingId }, booking);
            }
            return BadRequest();
        }

        /// <summary>
        /// Deleting a booking from the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBooking(int id)
        {
            bool success = await _service.DeleteBooking(id);
            if (!success)
            {
                return NotFound();
            }
            return NoContent();
        }

        /// <summary>
        /// Changes the status of the Booking
        /// </summary>
        /// <param name="id"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        [HttpPut("{id}/status")]
        public async Task<IActionResult> SetBookingStatus(int id, BookingStatusDTO status)
        {
            bool success = await _service.SetBookingStatus(id, (int)status.Status);
            if (!success)
            {
                return BadRequest();
            }
            return NoContent();
        }
    }
}