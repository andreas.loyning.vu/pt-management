﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PTManagementAPI.Models.Domain;
using PTManagementAPI.Models.DTO;
using PTManagementAPI.Models.DTO.Trainer;
using PTManagementAPI.Models.DTO.UserProfile;
using PTManagementAPI.Models.Enum;
using PTManagementAPI.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PTManagementAPI.Controllers
{
    [Route("api/v1/[controller]")]
    [Authorize("Trainer")]
    [ApiController]
    public class TrainersController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly TrainerService _service;
        private readonly ClientService _clientService;

        public TrainersController(TrainerService service, ClientService clientService, IMapper mapper)
        {
            _mapper = mapper;
            _service = service;
            _clientService = clientService;
        }

        /// <summary>
        /// Fetch all trainers
        /// </summary>
        /// <returns></returns>
        // GET: api/Trainers
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TrainerProfileViewDTO>>> GetTrainers()
        {
            // Get DMs
            var trainerList = await _service.GetTrainers();

            //Map to DTO
            ICollection<TrainerProfileDTO> trainerDTOList = _mapper.Map<ICollection<Trainer>, ICollection<TrainerProfileDTO>>(trainerList);

            foreach (TrainerProfileDTO t in trainerDTOList)
            {
                t.User = _mapper.Map<UserDTO>(t.User);
            }

            // Flatten by map
            ICollection<TrainerProfileViewDTO> trainerProfileViewDTOs = _mapper.Map<ICollection<TrainerProfileDTO>, ICollection<TrainerProfileViewDTO>>(trainerDTOList);

            // Validate list
            if (trainerProfileViewDTOs.Count > 0)
            {
                return Ok(trainerProfileViewDTOs);
            }
            return NoContent();
        }

        /// <summary>
        /// Fetch trainer by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>

        // GET: api/Trainers/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TrainerProfileViewDTO>> GetTrainer(int id)
        {
            System.Console.WriteLine(id);
            Trainer trainer = await _service.GetTrainer(id);

            TrainerProfileDTO trainerDTO = _mapper.Map<Trainer, TrainerProfileDTO>(trainer);
            trainerDTO.User = _mapper.Map<UserDTO>(trainerDTO.User);

            TrainerProfileViewDTO trainerProfileView = _mapper.Map<TrainerProfileDTO, TrainerProfileViewDTO>(trainerDTO);

            if (trainer is null)
            {
                return NotFound();
            }

            return trainerProfileView;
        }

        /// <summary>
        /// Update trainer userinformation by id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="trainerProfileDTO"></param>
        /// <returns></returns>

        // PUT: api/Trainers/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTrainerUser(int id, TrainerProfileViewDTO trainerProfileDTO)
        {
            if (id != trainerProfileDTO.TrainerId)
            {
                return BadRequest();
            }
            Trainer trainer = _mapper.Map<TrainerProfileDTO, Trainer>(_mapper.Map<TrainerProfileViewDTO, TrainerProfileDTO>(trainerProfileDTO));

            bool success = await _service.UpdateTrainerUser(id, trainer.User);
            if (!success)
            {
                return NotFound();
            }

            return NoContent();
        }

        /// <summary>
        /// Post new trainer
        /// </summary>
        /// <returns></returns>

        // POST: api/Trainers
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult<Trainer>> PostTrainer(TrainerPostDTO trainerPostDTO)
        {
            Trainer trainer = _mapper.Map<TrainerProfileDTO, Trainer>(_mapper.Map<TrainerPostDTO, TrainerProfileDTO>(trainerPostDTO));
            Trainer trainerPosted = await _service.PostTrainer(trainer);
            if (trainerPosted is null)
            {
                return Conflict();
            }

            return CreatedAtAction(nameof(GetTrainer), new { id = trainerPosted.TrainerId }, trainerPosted);
        }

        /// <summary>
        /// Fetch all clients by trainer
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>

        // GET: api/Trainers/{id}/clients
        // Trainer clients
        [HttpGet("{id}/clients")]
        public async Task<ActionResult<ICollection<TrainerClientDTO>>> GetTrainersClients(int id)
        {
            var clientsList = await _service.GetTrainersClients(id);
            //Trainer was not found
            if (clientsList is null)
            {
                return BadRequest();
            }
            ICollection<TrainerClientDTO> dtoList = new List<TrainerClientDTO>();
            foreach (Client client in clientsList)
            {
                //_mapper.Map<TrainerClientDTO>(client);
                TrainerClientDTO dto = _mapper.Map<TrainerClientDTO>(client.User);
                dto.ClientId = client.ClientId;
                dto.Sessions = client.Sessions;
                dtoList.Add(dto);
            }
            if (dtoList.Count > 0)
            {
                return Ok(dtoList);
            }
            return NoContent();
        }

        /// <summary>
        /// Fetch all bookings by trainer
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: api/Trainers/{id}/bookings
        // Trainer bookings
        [HttpGet("{id}/bookings")]
        public async Task<ActionResult<BookingDTO>> GetTrainersBookings(int id)
        {
            ICollection<BookingDTO> trainersBookings = _mapper.Map<ICollection<BookingDTO>>(await _service.GetTrainersBookings(id));
            if (trainersBookings.Count > 0)
            {
                return Ok(trainersBookings);
            }
            return NoContent();
        }

        /// <summary>
        /// Fetch all invoices by trainer
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: api/Trainers/{id}/invoices
        // Trainer invoices
        [HttpGet("{id}/invoices")]
        public async Task<ActionResult<ICollection<InvoiceDetailsDTO>>> GetTrainersInvoices(int id)
        {
            ICollection<InvoiceDetailsDTO> invoices = _mapper.Map<ICollection<InvoiceDetailsDTO>>(await _service.GetTrainersInvoices(id));
            foreach (InvoiceDetailsDTO invoice in invoices)
            {
                Client client = await _clientService.GetClient(invoice.ClientId);
                Trainer trainer = await _service.GetTrainer(invoice.TrainerId);
                invoice.ClientName = client.User.FirstName + " " + client.User.LastName;
                invoice.TrainerName = trainer.User.FirstName + " " + trainer.User.LastName;
            }
            if (invoices.Count > 0)
            {
                return Ok(invoices);
            }
            return NoContent();
        }

        // Disable / Enable trainer
        // Only for admin
        [HttpPut("{id}/enable")]
        [Authorize("Admin")]
        public async Task<ActionResult> EnableTrainer(int id)
        {
            bool success = await _service.EnableTrainer(id);
            if (!success)
            {
                return NotFound();
            }
            return NoContent();
        }

        [HttpPut("{id}/disable")]
        [Authorize("Admin")]
        public async Task<ActionResult> DisableTrainer(int id)
        {
            bool success = await _service.DisableTrainer(id);
            if (!success)
            {
                return NotFound();
            }
            return NoContent();
        }

        [HttpPut("{id}/active/{status}")]
        [Authorize("Admin")]
        public async Task<ActionResult> ChangeActiveStatus(int id, string status)
        {
            ActivationStatus statusEnum;
            if (Enum.TryParse<ActivationStatus>(status, out statusEnum))
            {
                bool success = await _service.UpdateActiveStatus(id, statusEnum);
                if (success)
                {
                    return NoContent();
                }

                return BadRequest();
            }

            return BadRequest();
        }
    }
}