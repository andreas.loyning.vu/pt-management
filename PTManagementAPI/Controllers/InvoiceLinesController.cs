﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PTManagementAPI.Models.Domain;
using PTManagementAPI.Models.DTO;
using PTManagementAPI.Models.DTO.InvoiceLine;
using PTManagementAPI.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PTManagementAPI.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class InvoiceLinesController : ControllerBase
    {
        private readonly InvoiceLineService _service;
        private readonly IMapper _mapper;

        public InvoiceLinesController(InvoiceLineService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        /// <summary>
        /// Fetches all Invoice Lines from the database
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize("Admin")]
        public async Task<ActionResult<IEnumerable<InvoiceLineDTO>>> GetInvoiceLines()
        {
            ICollection<InvoiceLineDTO> list = _mapper.Map<ICollection<InvoiceLineDTO>>(await _service.GetInvoiceLines());
            if (list.Count > 0)
            {
                return Ok(list);
            }
            return NoContent();
        }

        /// <summary>
        /// Fetches a single InvoiceLine
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [Authorize("Trainer")]

        public async Task<ActionResult<InvoiceLineDTO>> GetInvoiceLine(int id)
        {
            InvoiceLineDTO invoiceLine = _mapper.Map<InvoiceLineDTO>(await _service.GetInvoiceLine(id));
            if (invoiceLine == null)
            {
                return NotFound();
            }
            return invoiceLine;
        }

        /// <summary>
        /// Posts an InvoiceLine to the database
        /// </summary>
        /// <param name="invoiceLine"></param>
        /// <returns></returns>
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        [Authorize("Trainer")]

        public async Task<ActionResult<InvoiceLineDTO>> PostInvoiceLine(InvoiceLinePostDTO invoiceLine)
        {
            InvoiceLine invoiceLineDM = await _service.PostInvoiceLine(_mapper.Map<InvoiceLine>(invoiceLine));
            if (invoiceLineDM is not null)
            {
                InvoiceLineDTO invoiceLineDTO = _mapper.Map<InvoiceLineDTO>(invoiceLineDM);
                return CreatedAtAction(nameof(GetInvoiceLine), new { id = invoiceLineDM.InvoiceLineId }, invoiceLineDTO);
            }
            return BadRequest();
        }

        /// <summary>
        /// Updates an existing InvoiceLine
        /// </summary>
        /// <param name="id"></param>
        /// <param name="invoiceLine"></param>
        /// <returns></returns>
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        [Authorize("Trainer")]

        public async Task<ActionResult> PutInvoiceLine(int id, InvoiceLineDTO invoiceLine)
        {
            if (id != invoiceLine.InvoiceLineId)
            {
                return BadRequest();
            }
            await _service.UpdateInvoiceLine(id, _mapper.Map<InvoiceLine>(invoiceLine));
            return NoContent();
        }

        /// <summary>
        /// Deletes an invoice from the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [Authorize("Admin")]
        public async Task<ActionResult> DeleteInvoiceLine(int id)
        {
            bool success = await _service.DeleteInvoiceLine(id);
            if (!success)
            {
                return NotFound();
            }
            return NoContent();
        }
    }
}
