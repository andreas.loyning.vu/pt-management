﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PTManagementAPI.Models.Domain;
using PTManagementAPI.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PTManagementAPI.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class MailController : ControllerBase
    {
        private readonly MailService _service;

        public MailController(MailService service)
        {
            _service = service;
        }

        /// <summary>
        /// Get registration codes
        /// </summary>
        /// <returns></returns>
        [HttpGet("registration/codes")]
        [Authorize("Admin")]
        public async Task<ActionResult<IEnumerable<RegistrationCodes>>> GetCodes()
        {
            ICollection<RegistrationCodes> venues = await _service.GetRegistrationCodes();
            if (venues.Count > 0)
            {
                return Ok(venues);
            }
            return NoContent();
        }

        /// <summary>
        /// Post registration code
        /// </summary>
        /// <param name="registrationCode"></param>
        /// <returns></returns>
        [HttpPost("registration/codes")]
        [Authorize("Admin")]
        public async Task<ActionResult> PostCodes(RegistrationCodes registrationCode)
        {
            RegistrationCodes registrationCodeDM = await _service.PostRegistrationCodes(registrationCode);
            if (registrationCodeDM is null)
            {
                return BadRequest();
            }
            return CreatedAtAction(nameof(GetCodes), new { id = registrationCodeDM.Id }, registrationCodeDM);
        }

        /// <summary>
        /// Send registration link to client
        /// </summary>
        /// <param name="trainerId"></param>
        /// <param name="email"></param>
        /// <returns></returns>
        // Client entry-point
        [HttpGet("registration/client")]
        [Authorize("Trainer")]
        public async Task<IActionResult> SendRegistrationLink(int trainerId, string email)
        {
            bool success = await _service.SendRegistrationLink(trainerId, email);
            if (!success)
            {
                return BadRequest();
            }
            // fetch code
            return NoContent();
        }

        /// <summary>
        /// Entry point for client mail
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("registration/{id}")]
        public async Task<bool> RegisterClient(Guid id)
        {
            bool success = await _service.Registration(id);
            return success;
        }

        /// <summary>
        /// Send invoice by mail
        /// </summary>
        /// <param name="id"></param>
        /// <param name="email"></param>
        /// <returns></returns>
        [HttpGet("invoice/{id}")]
        [Authorize("Trainer")]
        public async Task<ActionResult> SendInvoiceByMail(int id, string email)
        {
            Invoice invoice = await _service.GetInvoice(id);
            byte[] invoicePdf = await _service.GeneratePdf(id);

            if (invoicePdf is null)
            {
                return NotFound();
            }

            //HTML body
            string htmlBody = $"<h1>Dear {invoice.Client.User.FirstName} </h1>"
                            + "Attached to this mail, you will find an invoice for your purchase.";

            await _service.SendEmailAsync(email, "PT-Management Invoice", htmlBody, invoicePdf, "invoice.pdf");

            return NoContent();
        }
    }
}