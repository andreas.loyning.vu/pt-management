using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PTManagementAPI.Models.Domain;
using PTManagementAPI.Models.DTO;
using PTManagementAPI.Models.DTO.Venue;
using PTManagementAPI.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PTManagementAPI.Controllers
{
    [Route("api/v1/[controller]")]
    [Authorize("Trainer")]
    [ApiController]
    public class VenuesController : ControllerBase
    {
        private readonly VenueService _service;
        private readonly IMapper _mapper;

        public VenuesController(VenueService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        /// <summary>
        /// Fetches all Venues
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<VenueDTO>>> GetVenues()
        {
            ICollection<VenueDTO> venues = _mapper.Map<ICollection<VenueDTO>>(await _service.GetVenues());
            if (venues.Count > 0)
            {
                return Ok(venues);
            }
            return NoContent();
        }

        /// <summary>
        /// Returns all venues that are active
        /// </summary>
        /// <returns></returns>
        [HttpGet("active")]
        public async Task<ActionResult<IEnumerable<VenueDTO>>> GetActiveVenues()
        {
            ICollection<VenueDTO> venues = _mapper.Map<ICollection<VenueDTO>>(await _service.GetActiveVenues());
            if (venues.Count > 0)
            {
                return Ok(venues);
            }
            return NoContent();
        }

        /// <summary>
        /// Returns all venues that are inactive
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("inactive")]
        public async Task<ActionResult<IEnumerable<VenueDTO>>> GetInActiveVenues()
        {
            ICollection<VenueDTO> venues = _mapper.Map<ICollection<VenueDTO>>(await _service.GetInActiveVenues());
            if (venues.Count > 0)
            {
                return Ok(venues);
            }
            return NoContent();
        }

        /// <summary>
        /// Fetches a single Venue with the specified Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<VenueDTO>> GetVenue(int id)
        {
            VenueDTO venue = _mapper.Map<VenueDTO>(await _service.GetVenue(id));
            if (venue is null)
            {
                return NotFound();
            }
            return venue;
        }

        /// <summary>
        /// Adds a new Venue
        /// </summary>
        /// <param name="venue"></param>
        /// <returns></returns>
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult> PostVenue(VenuePostDTO venue)
        {
            Venue venueDM = await _service.PostVenue(_mapper.Map<Venue>(venue));
            if (venue is null)
            {
                return BadRequest();
            }
            VenueDTO venueDTO = _mapper.Map<VenueDTO>(venueDM);
            return CreatedAtAction(nameof(GetVenue), new { id = venueDTO.VenueId }, venueDTO);
        }

        /// <summary>
        /// Updates the specified Venue
        /// </summary>
        /// <param name="id"></param>
        /// <param name="venue"></param>
        /// <returns></returns>
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<ActionResult> PutVenue(int id, VenueDTO venue)
        {
            if (id != venue.VenueId)
            {
                return BadRequest();
            }
            bool success = await _service.UpdateVenue(id, _mapper.Map<Venue>(venue));
            if (!success)
            {
                return NotFound();
            }
            return NoContent();
        }

        /// <summary>
        /// Disables a Venue
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPut("{id}/disable")]
        public async Task<ActionResult> DisableVenue(int id)
        {
            bool success = await _service.DisableVenue(id);
            if (!success)
            {
                return NotFound();
            }
            return NoContent();
        }

        /// <summary>
        /// Enables a Venue
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPut("{id}/enable")]
        public async Task<ActionResult> EnableVenue(int id)
        {
            bool success = await _service.EnableVenue(id);
            if (!success)
            {
                return NotFound();
            }
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteVenue(int id)
        {
            bool success = await _service.DeleteVenue(id);
            if (!success)
            {
                return NotFound();
            }
            return NoContent();
        }
    }
}