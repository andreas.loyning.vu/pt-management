﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PTManagementAPI.Models.Domain;
using PTManagementAPI.Models.Enum;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PTManagementAPI.Services
{
    public class TrainerService
    {
        private readonly PTDBContext _context;

        public TrainerService(PTDBContext context, IMapper mapper)
        {
            _context = context;
        }

        /// <summary>
        /// Fetch view for all trainers
        /// </summary>
        /// <returns></returns>
        public async Task<ICollection<Trainer>> GetTrainers()
        {
            ICollection<Trainer> trainerDMList = await _context.Trainers.ToListAsync();
            return trainerDMList;
        }
        /// <summary>
        /// Fetch trainerview by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Trainer> GetTrainer(int id)
        {
            Trainer trainerDM = await _context.Trainers.FindAsync(id);

            return trainerDM;
        }

        /// <summary>
        /// Post new trainer
        /// </summary>
        /// <param name="trainer"></param>
        /// <returns></returns>
        public async Task<Trainer> PostTrainer(Trainer trainer)
        {
            //Trainer trainerDM = _mapper.Map<TrainerProfileDTO, Trainer>(_mapper.Map<TrainerProfileViewDTO,TrainerProfileDTO>(trainer));
            if (await _context.Users.FirstOrDefaultAsync(u => u.Subject == trainer.User.Subject) is not null)
            {
                return null;
            }

            // Set role and active default values
            trainer.User.Role = UserRole.Trainer;
            trainer.User.IsActive = false;

            _context.Trainers.Add(trainer);
            await _context.SaveChangesAsync();
            return trainer;
        }



        /// <summary>
        /// Update userinfo for trainer
        /// </summary>
        /// <param name="id"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<bool> UpdateTrainerUser(int id, User user)
        {
            Trainer trainer = await _context.Trainers.Include(t => t.User).AsNoTracking().FirstOrDefaultAsync(t => t.TrainerId == id);
            user.UserId = trainer.UserId;
            user.Subject = trainer.User.Subject;
            user.Role = UserRole.Trainer;
            _context.Entry(user).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
                return true;
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TrainerExists(id))
                {
                    return false;
                }
                else
                {
                    throw;
                }
            }
        }

        // require adminrights?

        /// <summary>
        /// Disable trainer
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<bool> DisableTrainer(int id)
        {
            if (TrainerExists(id))
            {
                Trainer trainer = await _context.Trainers.FindAsync(id);
                trainer.User.IsActive = false;
                await _context.SaveChangesAsync();
                return true;
            }

            return false;
        }

        public async Task<bool> EnableTrainer(int id)
        {
            if (TrainerExists(id))
            {
                Trainer trainer = await _context.Trainers.FindAsync(id);
                trainer.User.IsActive = true;
                await _context.SaveChangesAsync();
                return true;
            }

            return false;
        }

        // require adminrights?

        /// <summary>
        /// Enable trainer
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>

        private bool TrainerExists(int id)
        {
            return _context.Trainers.Any(e => e.TrainerId == id);
        }


        public async Task<ICollection<Booking>> GetTrainersBookings(int id)
        {
            ICollection<Booking> trainersBookingList = await _context.Bookings.Where(c => c.TrainerId == id).ToListAsync();
            return trainersBookingList;
        }

        public async Task<ICollection<Client>> GetTrainersClients(int id)
        {
            Trainer trainer = await _context.Trainers.FindAsync(id);
            if (trainer is null)
            {
                return null;
            }
            ICollection<Client> trainersClients = await _context.Clients.Where(c => c.TrainerId == id).ToListAsync();
            return trainersClients;
        }

        public async Task<ICollection<Invoice>> GetTrainersInvoices(int id)
        {
            Trainer trainer = await _context.Trainers.FindAsync(id);
            if (trainer is null)
            {
                return null;
            }
            ICollection<Invoice> trainersInvoices = await _context.Invoices.Where(i => i.TrainerId == id).ToListAsync();
            return trainersInvoices;
        }

        public async Task<bool> UpdateActiveStatus(int id, ActivationStatus status)
        {
            Trainer trainer = await _context.Trainers.FindAsync(id);
            if (trainer is null)
            {
                return false;
            }

            trainer.ActivationStatus = status;
            await _context.SaveChangesAsync();
            return true;
        }


    }
}
