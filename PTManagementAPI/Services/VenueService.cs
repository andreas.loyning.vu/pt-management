﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PTManagementAPI.Models.Domain;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PTManagementAPI.Services
{
    public class VenueService
    {
        private readonly PTDBContext _context;

        public VenueService(PTDBContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Retrieves all Venues from the database
        /// </summary>
        /// <returns></returns>
        public async Task<ICollection<Venue>> GetVenues()
        {
            ICollection<Venue> venues = await _context.Venues.ToListAsync();
            return venues;
        }

        /// <summary>
        /// Returns all active venues
        /// </summary>
        /// <returns></returns>
        public async Task<ICollection<Venue>> GetActiveVenues()
        {
            ICollection<Venue> venues = await _context.Venues.Where(v => v.IsActive == true).ToListAsync();
            return venues;
        }

        /// <summary>
        /// Returns all inactive venues
        /// </summary>
        /// <returns></returns>
        public async Task<ICollection<Venue>> GetInActiveVenues()
        {
            ICollection<Venue> venues = await _context.Venues.Where(v => v.IsActive == false).ToListAsync();
            return venues;
        }


        /// <summary>
        /// Retrieves a single Venue from the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Venue> GetVenue(int id)
        {
            Venue venue = await _context.Venues.FindAsync(id);
            return venue;
        }

        /// <summary>
        /// Posts a venue to the database
        /// </summary>
        /// <param name="venue"></param>
        /// <returns></returns>
        public async Task<Venue> PostVenue(Venue venue)
        {
            City city = await _context.Cities.FindAsync(venue.PostalCode);
            if(city is null)
            {
                return null;
            }
            _context.Venues.Add(venue);
            await _context.SaveChangesAsync();
            return venue;
        }

        /// <summary>
        /// Updates the specified venue
        /// </summary>
        /// <param name="id"></param>
        /// <param name="venue"></param>
        /// <returns></returns>
        public async Task<bool> UpdateVenue(int id, Venue venue)
        {
            _context.Entry(venue).State = EntityState.Modified;
            try
            {
                await _context.SaveChangesAsync();
                return true;
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!VenueExists(id))
                {
                    return false;
                }
                else
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Disables a venue from the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<bool> DisableVenue(int id)
        {
            if (VenueExists(id))
            {
                Venue venueDM = await _context.Venues.FindAsync(id);
                venueDM.IsActive = false;
                await _context.SaveChangesAsync();
                return true;
            }
            return false;
        }

        /// <summary>
        /// Enables a venue from the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<bool> EnableVenue(int id)
        {
            if (VenueExists(id))
            {
                Venue venueDM = await _context.Venues.FindAsync(id);
                venueDM.IsActive = true;
                await _context.SaveChangesAsync();
                return true;
            }
            return false;
        }

        public async Task<bool> DeleteVenue(int id)
        {
            Venue venueDM = await _context.Venues.FindAsync(id);
            if (venueDM is null)
            {
                return false;
            }
            _context.Venues.Remove(venueDM);
            await _context.SaveChangesAsync();
            return true;
        }

        private bool VenueExists(int id)
        {
            return _context.Venues.Any(e => e.VenueId == id);
        }
    }
}
