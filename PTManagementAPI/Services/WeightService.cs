using Microsoft.EntityFrameworkCore;
using PTManagementAPI.Models.Domain;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PTManagementAPI.Services
{
    public class WeightService
    {
        public readonly PTDBContext _context;
        public WeightService(PTDBContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Fetches all Weights from the database
        /// </summary>
        /// <returns></returns>
        public async Task<ICollection<Weight>> GetWeights()
        {
            return await _context.Weights.ToListAsync();
        }

        /// <summary>
        /// Fetches a single Weight from the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Weight> GetWeight(int id)
        {
            return await _context.Weights.FindAsync(id);
        }

        /// <summary>
        /// Posts an Weight to the database
        /// </summary>
        /// <param name="weight">weight to be added</param>
        /// <returns></returns>
        public async Task<Weight> PostWeight(Weight weight)
        {
            Client clientDM = await _context.Clients.FindAsync(weight.ClientId);
            if (clientDM is null)
            {
                return null;
            }
            _context.Weights.Add(weight);
            await _context.SaveChangesAsync();

            return weight;
        }

        /// <summary>
        /// Updates the specified Weight
        /// </summary>
        /// <param name="id">Id of the weight</param>
        /// <param name="weight">Updated weight information</param>
        public async Task<bool> UpdateWeight(int id, Weight weight)
        {
            Client clientDM = await _context.Clients.FindAsync(weight.ClientId);
            if (clientDM is null)
            {
                return false;
            }

            _context.Entry(weight).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
                return true;
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!WeightExists(id))
                {
                    return false;
                }
                else
                {
                    throw;
                }
            }
        }


        /// <summary>
        /// Deletes an existing weight
        /// </summary>
        /// <param name="id">Id of the weight to be deleted</param>
        /// <returns></returns>
        public async Task<bool> DeleteWeight(int id)
        {
            if (WeightExists(id))
            {
                Weight weightDM = await _context.Weights.FindAsync(id);
                _context.Weights.Remove(weightDM);
                await _context.SaveChangesAsync();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Returns all the weights from a certain client
        /// </summary>
        /// <param name="id">id of the clinet</param>
        /// <returns></returns>
        public async Task<ICollection<Weight>> GetClientsWeights(int id) {
            ICollection<Weight> weights = await _context.Weights.Where(w => w.ClientId == id).ToListAsync();
            return weights;
        }

        /* /// <summary>
        /// Gets all weights from a client id
        /// </summary>
        /// <param name="id">id of the client</param>
        /// <returns></returns>
        public async Task<ICollection<WeightGetDTO>> WeightsOfClient(int id)
        {
            ICollection<Weight> weigthsDM = await _context.Weights.Where(w => w.ClientId == id).ToListAsync();
            ICollection<WeightGetDTO> weightsDTO = _mapper.Map<ICollection<WeightGetDTO>>(weigthsDM);
            return weightsDTO;
        } */

        
        private bool WeightExists(int id)
        {
            return _context.Weights.Any(w => w.WeightId == id);
        }
    }
}