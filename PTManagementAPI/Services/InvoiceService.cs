using Microsoft.EntityFrameworkCore;
using PdfSharpCore.Pdf;
using PTManagementAPI.Models.Domain;
using PTManagementAPI.Models.Enum;
using PTManagementAPI.Utils.PdfUtils;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace PTManagementAPI.Services
{
    public class InvoiceService
    {
        public readonly PTDBContext _context;
        public InvoiceService(PTDBContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Fetches all Invoices from the databse
        /// </summary>
        /// <returns></returns>
        public async Task<ICollection<Invoice>> GetInvoices()
        {
            return await _context.Invoices.ToListAsync();
        }

        /// <summary>
        /// Fetches a single Invoice from the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Invoice> GetInvoice(int id)
        {
            return await _context.Invoices.FindAsync(id);
        }

        /// <summary>
        /// Posts an Invoice to the database
        /// </summary>
        /// <param name="invoice">Invoice to be added</param>
        /// <returns></returns>
        public async Task<Invoice> PostInvoice(Invoice invoice)
        {
            Client clientDM = await _context.Clients.FindAsync(invoice.ClientId);
            Trainer trainerDM = await _context.Trainers.FindAsync(invoice.TrainerId);
            if (clientDM is null || trainerDM is null)
            {
                return null;
            }
            _context.Invoices.Add(invoice);
            await _context.SaveChangesAsync();
            return invoice;
        }

        /// <summary>
        /// Updates the specified Invoice
        /// </summary>
        /// <param name="id">Id of the invoice</param>
        /// <param name="invoice">Updated invoice information</param>
        public async Task<bool> UpdateInvoice(int id, Invoice invoice)
        {
            Client clientDM = await _context.Clients.FindAsync(invoice.ClientId);
            Trainer trainerDM = await _context.Trainers.FindAsync(invoice.TrainerId);
            if (clientDM is null || trainerDM is null)
            {
                return false;
            }

            _context.Entry(invoice).State = EntityState.Modified;
            try
            {
                await _context.SaveChangesAsync();
                return true;
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!InvoiceExists(id))
                {
                    return false;
                }
                else
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Sets the status of the invoice to the specified status
        /// </summary>
        /// <param name="id">Id of the invoice</param>
        /// <param name="status">InvoiceStatus</param>
        /// <returns></returns>
        public async Task<bool> SetInvoiceStatus(int id, InvoiceStatus status)
        {
            if (InvoiceExists(id))
            {
                Invoice invoiceDM = await _context.Invoices.FindAsync(id);
                invoiceDM.Status = status;
                await _context.SaveChangesAsync();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Deletes an existing invoice
        /// </summary>
        /// <param name="id">Id of the invoice to be deleted</param>
        /// <returns></returns>
        public async Task<bool> DeleteInvoice(int id)
        {
            if (InvoiceExists(id))
            {
                Invoice invoiceDM = await _context.Invoices.FindAsync(id);
                _context.Invoices.Remove(invoiceDM);
                await _context.SaveChangesAsync();
                return true;
            }

            return false;
        }

        public async Task<bool> AddInvoiceLineToInvoice(int invoiceId, ICollection<int> invoiceLineIds)
        {
            Invoice invoiceDM = await _context.Invoices.FindAsync(invoiceId);
            foreach (int invoiceLineId in invoiceLineIds)
            {
                InvoiceLine invoiceLineDM = await _context.InvoiceLines.FindAsync(invoiceLineId);
                if (invoiceLineDM is null || invoiceDM is null)
                {
                    return false;
                }

                invoiceDM.InvoiceLines.Add(invoiceLineDM);
            }

            await _context.SaveChangesAsync();
            return true;
        }

        /// <summary>
        /// Returns a list of all invoice Lines that belong to an invoice
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ICollection<InvoiceLine>> GetInvoiceLinesForInvoice(int id)
        {
            Invoice invoice = await _context.Invoices.FindAsync(id);
            if(invoice is null)
            {
                return null;
            }
            return await _context.InvoiceLines.Where(i => i.InvoiceId == id).ToListAsync();
        }

        private bool InvoiceExists(int id)
        {
            return _context.Invoices.Any(i => i.InvoiceId == id);
        }

        public async Task<byte[]> GeneratePdf(int id)
        {

            if (!InvoiceExists(id))
            {
                return null;
            }
            // Fetch information needed for invoice
            Invoice invoice =  _context.Invoices.Find(id); // Might need to change to async later
            Trainer trainer = invoice.Trainer;
            Client client = invoice.Client;
            ICollection<InvoiceLine> invoiceLines = invoice.InvoiceLines;

            // Generate pdf document
            PdfDocument document = PdfUtils.CreateDocument(trainer, invoice, client);

            // Create memory stream in order to save and return pdf as byteArray
            MemoryStream m = new MemoryStream();

            document.Save(m);
            document.Close();



            // Convert a C# string to a byte array  
            byte[] byteArray = m.ToArray();
            return byteArray;
        }
    }


}


