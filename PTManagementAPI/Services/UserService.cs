using Microsoft.EntityFrameworkCore;
using PTManagementAPI.Models.Domain;
using PTManagementAPI.Models.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PTManagementAPI.Services
{
    public class UserService
    {
        private readonly PTDBContext _context;
        public UserService(PTDBContext context)
        {
            _context = context;
        }

        public User GetUser(string subject)
        {
            User userDM = _context.Users.FirstOrDefault(user => user.Subject == subject);
            return userDM;
        }

        public async Task<Trainer> GetTrainer(User user)
        {
            return await _context.Trainers.FirstOrDefaultAsync(t => t.User.Subject == user.Subject);
        }
        public async Task<Client> GetClient(User user)
        {
            return await _context.Clients.FirstOrDefaultAsync(c => c.User.Subject == user.Subject);
        }

        public async Task<Admin> GetAdmin(User user)
        {
            return await _context.Admins.FirstOrDefaultAsync(a => a.User.Subject == user.Subject);
        }
    }

}