﻿using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using MimeKit;
using PTManagementAPI.Models.Domain;
using PTManagementAPI.Models.Email;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PdfSharpCore.Pdf;
using PTManagementAPI.Utils.PdfUtils;
using System.IO;

namespace PTManagementAPI.Services
{
    public class MailService
    {


        private readonly SmtpSettings _smtpSettings;
        private readonly IWebHostEnvironment _env;
        private readonly PTDBContext _context;

        public MailService(IOptions<SmtpSettings> smtpSettings, IWebHostEnvironment env, PTDBContext context)
        {
            _smtpSettings = smtpSettings.Value;
            _env = env;
            _context = context;
        }

        public Task<bool> GetRegistrationCode(Guid id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get all registration codes, should only be available to admin user
        /// </summary>
        /// <returns></returns>
        public async Task<ICollection<RegistrationCodes>> GetRegistrationCodes()
        {
            ICollection<RegistrationCodes> registrationCodes = await _context.RegistrationCodes.ToListAsync(); 
            return registrationCodes;
        }

        /// <summary>
        /// Post registration code, should only be available to admin
        /// </summary>
        /// <param name="registrationCode"></param>
        /// <returns></returns>
        public async Task<RegistrationCodes> PostRegistrationCodes(RegistrationCodes registrationCode)
        {
            if (!(registrationCode.TrainerId is 0))
            {
                Trainer trainer = await _context.Trainers.FindAsync(registrationCode.TrainerId);
                if (trainer is null)
                {
                    return null;
                }
            }
            _context.RegistrationCodes.Add(registrationCode);
            await _context.SaveChangesAsync();
            return registrationCode;
        }

        /// <summary>
        /// Send email with html body and subject
        /// </summary>
        /// <param name="email"> Email to send to</param>
        /// <param name="subject"> Subject of email</param>
        /// <param name="body"> body of mail, which accepts html</param>
        /// <returns></returns>
        public async Task SendEmailAsync(string email, string subject, string body)
        {
            try
            {
                var message = new MimeMessage();
                message.From.Add(new MailboxAddress(_smtpSettings.SenderName, _smtpSettings.SenderEmail));
                // Obsolete? maybe change
                message.To.Add(new MailboxAddress(email));
                message.Subject = subject;
                message.Body = new TextPart("html")
                {
                    Text = body
                };

                using (var client = new SmtpClient())
                {

                    client.ServerCertificateValidationCallback = (s, c, h, e) => true; 

                    if (_env.IsDevelopment())
                    {

                        await client.ConnectAsync(_smtpSettings.Server, _smtpSettings.Port, SecureSocketOptions.StartTls);
                    }
                    else
                    {
                        await client.ConnectAsync(_smtpSettings.Server);
                    }
                    await client.AuthenticateAsync(_smtpSettings.Username, _smtpSettings.Password);
                    await client.SendAsync(message);
                    await client.DisconnectAsync(true);
                }
            }
            catch (Exception e)
            {
                throw new InvalidOperationException(e.Message);
            }
        }

        /// <summary>
        /// Send email with html body, subject and a bytefile as attachment
        /// </summary>
        /// <param name="email">Email to send to</param>
        /// <param name="subject">Subject of email</param>
        /// <param name="body">body of mail, which accepts html</param>
        /// <param name="file">Attachment to be sent with email, as byte[] array</param>
        /// <param name="filename">filename</param>
        /// <returns></returns>
        public async Task SendEmailAsync(string email, string subject, string body, byte[] file, string filename)
        {
            try
            {
                var message = new MimeMessage();
                message.From.Add(new MailboxAddress(_smtpSettings.SenderName, _smtpSettings.SenderEmail));
                // Obsolete? maybe change
                message.To.Add(new MailboxAddress(email));
                message.Subject = subject;

                var emailBody = new MimeKit.BodyBuilder
                {
                    HtmlBody = body
                };


                // Add attachment
                emailBody.Attachments.Add(filename, file);
                message.Body = emailBody.ToMessageBody();
                using (var client = new SmtpClient())
                {

                    client.ServerCertificateValidationCallback = (s, c, h, e) => true; 

                    if (_env.IsDevelopment())
                    {

                        await client.ConnectAsync(_smtpSettings.Server, _smtpSettings.Port, SecureSocketOptions.StartTls);
                    }
                    else
                    {
                        await client.ConnectAsync(_smtpSettings.Server);
                    }
                    // This might need to change
                    await client.AuthenticateAsync(_smtpSettings.Username, _smtpSettings.Password);
                    await client.SendAsync(message);
                    await client.DisconnectAsync(true);
                }
            }
            catch (Exception e)
            {
                throw new InvalidOperationException(e.Message);
            }
        }

        /// <summary>
        /// Send mail containing a registration link
        /// </summary>
        /// <param name="email">Email to send to</param>
        /// <returns></returns>
        public async Task<bool> SendRegistrationLink(int trainerId, string email)
        {

            Trainer trainer = await _context.Trainers.FindAsync(trainerId);
            User user = await _context.Users.FirstOrDefaultAsync(u => u.Email == email);
            if (trainer is null || user is not null)
            {
                return false;
            }
            RegistrationCodes registrationCode = new RegistrationCodes { TrainerId = trainerId, IssueDate = DateTime.Now, isActive = true };

            

            _context.RegistrationCodes.Add(registrationCode);
            await _context.SaveChangesAsync();


            //TODO: Update this url to the public one
            string body = $"Hello, please register your user by clicking <a href='https://ptmanagement.azurewebsites.net/register/{registrationCode.Id}'>This link</a>";
            string subject = "PT-Management - Client Registration";

            await SendEmailAsync(email, subject, body);
            return true;

        }

        /// <summary>
        /// Validate registration code used
        /// </summary>
        /// <param name="id">Guid ID for registration code</param>
        /// <returns></returns>
        public async Task<bool> Registration(Guid id)
        {
            // Check if registration code exists
            if (RegistrationCodeExists(id))
            {
                // Find registration code
                RegistrationCodes registrationCode = await _context.RegistrationCodes.FindAsync(id);

                return registrationCode.isActive;
                // Code active -- not used
                /* if (registrationCode.isActive)
                {
                    registrationCode.isActive = false;
                    await _context.SaveChangesAsync();
                    return true;
                }
                // Code is used, or set to false due to time constraint
                else
                {
                    return false;
                } */

            }

            return false;
        }

        /// <summary>
        /// Check if registration code exists
        /// </summary>
        /// <param name="id">id of registration code</param>
        /// <returns></returns>
        private bool RegistrationCodeExists(Guid id)
        {
            return _context.RegistrationCodes.Any(e => e.Id == id);

        }

        /// <summary>
        /// Fetches a single Invoice from the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Invoice> GetInvoice(int id)
        {
            return await _context.Invoices.FindAsync(id);
        }

        private bool InvoiceExists(int id)
        {
            return _context.Invoices.Any(i => i.InvoiceId == id);
        }

        public async Task<byte[]> GeneratePdf(int id)
        {

            if (!InvoiceExists(id))
            {
                return null;
            }
            // Fetch information needed for invoice
            Invoice invoice = _context.Invoices.Find(id); // Might need to change to async later
            Trainer trainer = invoice.Trainer;
            Client client = invoice.Client;

            // Generate pdf document
            PdfDocument document = PdfUtils.CreateDocument(trainer, invoice, client);

            // Create memory stream in order to save and return pdf as byteArray
            MemoryStream m = new MemoryStream();

            document.Save(m);
            document.Close();



            // Convert a C# string to a byte array  
            byte[] byteArray = m.ToArray();
            return byteArray;
        }
    }
}
