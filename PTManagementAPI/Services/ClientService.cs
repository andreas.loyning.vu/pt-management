﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PTManagementAPI.Models.Domain;
using PTManagementAPI.Models.DTO;
using PTManagementAPI.Models.DTO.Client;
using PTManagementAPI.Models.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PTManagementAPI.Services
{
    public class ClientService
    {
        private readonly PTDBContext _context;

        public ClientService(PTDBContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Retrieves all Client from the database
        /// </summary>
        /// <returns></returns>
        public async Task<ICollection<Client>> GetClients()
        {
            return await _context.Clients.ToListAsync();
        }

        /// <summary>
        /// Retrieves a single Client from the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Client> GetClient(int id)
        {
            var client = await _context.Clients.FindAsync(id);

            return client;
        }

        /// <summary>
        /// Posts a client to the database
        /// </summary>
        /// <param name="client"></param>
        /// <returns></returns>
        public async Task<Client> PostClient(Client client)
        {
            try
            {
                _context.Users.Add(client.User);
                _context.Clients.Add(client);
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return null;
            }

            return client;
        }

        /// <summary>
        /// Updates the specified Clients user info
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userDM"></param>
        /// <returns></returns>
        public async Task<bool> UpdateClientUser(int id, User userDM)
        {
            var client = await _context.Clients.Include(c => c.User).AsNoTracking().FirstOrDefaultAsync(c => c.ClientId == id);
            userDM.UserId = client.UserId;
            userDM.Subject = client.User.Subject;
            userDM.Role = UserRole.Client;
            _context.Entry(userDM).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
                return true;
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ClientExists(id))
                {
                    return false;
                }
                else
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Updates the specified Client info
        /// </summary>
        /// <param name="id"></param>
        /// <param name="client"></param>
        /// <returns></returns>
        public async Task<bool> UpdateClient(int id, Client client)
        {
            _context.Entry(client).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
                return true;
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ClientExists(id))
                {
                    return false;
                }
                else
                {
                    throw;
                }
            }
        }

        public async void DeactivateGuid(Guid guid)
        {
            RegistrationCodes code = await _context.RegistrationCodes.FindAsync(guid);
            code.isActive = false;
            await _context.SaveChangesAsync();
        }

        public async Task<RegistrationCodes> GetGuid(Guid id)
        {
            RegistrationCodes code = await _context.RegistrationCodes.FindAsync(id);
            return code;
        }


        private bool ClientExists(int id)
        {
            return _context.Clients.Any(e => e.ClientId == id);
        }


    }
}
