﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PTManagementAPI.Models.Domain;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PTManagementAPI.Services
{
    public class InvoiceLineService
    {
        private readonly PTDBContext _context;

        public InvoiceLineService(PTDBContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Get all Invoices
        /// </summary>
        /// <returns></returns>
        public async Task<ICollection<InvoiceLine>> GetInvoiceLines()
        {
            return await _context.InvoiceLines.ToListAsync();
        }

        /// <summary>
        /// Get a specific InvoiceLine by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<InvoiceLine> GetInvoiceLine(int id)
        {
            return await _context.InvoiceLines.FindAsync(id);
        }

        /// <summary>
        /// Creates a new invoice
        /// </summary>
        /// <param name="invoiceLine"></param>
        /// <returns></returns>
        public async Task<InvoiceLine> PostInvoiceLine(InvoiceLine invoiceLine)
        {
            Invoice invoice = await _context.Invoices.FindAsync(invoiceLine.InvoiceId);
            Client client = await _context.Clients.FindAsync(invoice.ClientId);
            if (invoice is null || client is null)
            {
                return null;
            }
            client.Sessions += invoiceLine.Quantity;
            await _context.InvoiceLines.AddAsync(invoiceLine);
            await _context.SaveChangesAsync();
            return invoiceLine;
        }

        /// <summary>
        /// Updates an InvoiceLine
        /// </summary>
        /// <param name="id"></param>
        /// <param name="invoiceLine"></param>
        /// <returns></returns>
        public async Task<bool> UpdateInvoiceLine(int id, InvoiceLine invoiceLine)
        {
            _context.Entry(invoiceLine).State = EntityState.Modified;
            try
            {
                await _context.SaveChangesAsync();
                return true;
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!InvoiceLineExists(id))
                {
                    return false;
                }
                else
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Deletes InvoiceLine
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<bool> DeleteInvoiceLine(int id)
        {
            InvoiceLine invoiceLineDM = await _context.InvoiceLines.FindAsync(id);
            if (invoiceLineDM is null)
            {
                return false;
            }
            _context.InvoiceLines.Remove(invoiceLineDM);
            await _context.SaveChangesAsync();
            return true;
        }

        private bool InvoiceLineExists(int id)
        {
            return _context.InvoiceLines.Any(e => e.InvoiceLineId == id);
        }
    }
}
