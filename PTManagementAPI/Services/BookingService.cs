﻿using Microsoft.EntityFrameworkCore;
using PTManagementAPI.Models.Domain;
using PTManagementAPI.Models.Enum;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PTManagementAPI.Services
{
    public class BookingService
    {
        private readonly PTDBContext _context;

        public BookingService(PTDBContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Fetches all Bookings
        /// </summary>
        /// <returns>ICollection of Booking</returns>
        public async Task<ICollection<Booking>> GetBookings()
        {
            ICollection<Booking> bookingList = await _context.Bookings.ToListAsync();
            return bookingList;
        }

        /// <summary>
        /// Fetches a single Booking
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Booking> GetBooking(int id)
        {
            Booking booking = await _context.Bookings.FindAsync(id);
            return booking;
        }

        /// <summary>
        /// Gets All bookings for a client
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ICollection<Booking>> GetClientsBookings(int id)
        {
            ICollection<Booking> clientsBookingList = await _context.Bookings.Where(c => c.ClientId == id).ToListAsync();
            return clientsBookingList;
        }

        /// <summary>
        /// Gets All bookings for a Trainer
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ICollection<Booking>> GetTrainersBookings(int id)
        {
            ICollection<Booking> trainersBookingList = await _context.Bookings.Where(c => c.TrainerId == id).ToListAsync();
            return trainersBookingList;
        }

        /// <summary>
        /// Updates a booking
        /// </summary>
        /// <param name="id"></param>
        /// <param name="booking"></param>
        /// <returns></returns>
        public async Task<bool> UpdateBooking(int id, Booking booking)
        {
            _context.Entry(booking).State = EntityState.Modified;
            Client client = await _context.Clients.FindAsync(booking.ClientId);
            if(client is null)
            {
                return false;
            }
            else
            {
                if(booking.Status != BookingStatus.Booked)
                {
                    sessionUpdate(client, booking.Status);
                }
            }
            try
            {
                await _context.SaveChangesAsync();
                return true;
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BookingExists(id))
                {
                    return false;
                }
                else
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Adds a new booking to the database
        /// </summary>
        /// <param name="booking"></param>
        /// <returns></returns>
        public async Task<Booking> PostBooking(Booking booking)
        {
            Trainer trainer = await _context.Trainers.FindAsync(booking.TrainerId);
            Client client = await _context.Clients.FindAsync(booking.ClientId);
            Venue venue = await _context.Venues.FindAsync(booking.VenueId);
            if (client is null || trainer is null || venue is null)
            {
                return null;
            }
            await _context.Bookings.AddAsync(booking);
            await _context.SaveChangesAsync();
            return booking;
        }

        /// <summary>
        /// Deletes a booking from the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<bool> DeleteBooking(int id)
        {
            Booking booking = await _context.Bookings.FindAsync(id);
            if (booking is null)
            {
                return false;
            }
            _context.Bookings.Remove(booking);
            await _context.SaveChangesAsync();
            return true;
        }

        /// <summary>
        /// Sets the Booking Status
        /// </summary>
        /// <param name="id"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public async Task<bool> SetBookingStatus(int id, int status)
        {
            Booking booking = await _context.Bookings.FindAsync(id);
            if (booking is null)
            {
                return false;
            }
            booking.Status = (BookingStatus)status;
            await _context.SaveChangesAsync();
            return true;
        }

        private bool BookingExists(int id)
        {
            return _context.Bookings.Any(s => s.BookingId == id);
        }

        private void sessionUpdate(Client client, BookingStatus status)
        {
            if(status == BookingStatus.Cancelled)
            {
                client.Sessions += 1;
            }
            else
            {
                client.Sessions -= 1;
            }
        }
    }
}