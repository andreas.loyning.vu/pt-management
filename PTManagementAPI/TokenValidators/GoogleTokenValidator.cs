using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Google.Apis.Auth;
using Microsoft.IdentityModel.Tokens;
using PTManagementAPI.Models.Enum;
using PTManagementAPI.Services;
using PTManagementAPI.Models.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;

namespace PTManagementAPI.TokenValidators
{
    public class GoogleTokenValidator : ISecurityTokenValidator
    {
        private readonly JwtSecurityTokenHandler _tokenHandler;
        private readonly UserService _service;

        public GoogleTokenValidator(UserService service)
        {
            _service = service;
            _tokenHandler = new JwtSecurityTokenHandler();
        }

        public bool CanValidateToken => true;

        public int MaximumTokenSizeInBytes { get; set; } = TokenValidationParameters.DefaultMaximumTokenSizeInBytes;

        public bool CanReadToken(string securityToken)
        {
            return _tokenHandler.CanReadToken(securityToken);
        }

        public ClaimsPrincipal ValidateToken(string securityToken, TokenValidationParameters validationParameters, out SecurityToken validatedToken)
        {
            validatedToken = null;
            var payload = GoogleJsonWebSignature.ValidateAsync(securityToken, new GoogleJsonWebSignature.ValidationSettings()).Result; // here is where I delegate to Google to validate

            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, payload.Name),
                new Claim(ClaimTypes.Name, payload.Name),
                new Claim(JwtRegisteredClaimNames.GivenName, payload.GivenName),
                new Claim(JwtRegisteredClaimNames.Email, payload.Email),
                new Claim(JwtRegisteredClaimNames.Sub, payload.Subject),
                new Claim(JwtRegisteredClaimNames.Iss, payload.Issuer),
            };

            // Try to add role
            User user = _service.GetUser(payload.Subject);
            if (user is not null)
            {
                switch (user.Role)
                {
                    case UserRole.Admin:
                        {
                            claims.Add(new Claim(UserRole.Admin.ToString().ToLower(), "true"));
                            claims.Add(new Claim(UserRole.Trainer.ToString().ToLower(), "true"));
                            claims.Add(new Claim(UserRole.Client.ToString().ToLower(), "true"));
                            claims.Add(new Claim("weight", "true"));
                            break;
                        }
                    case UserRole.Trainer:
                        {
                            claims.Add(new Claim(UserRole.Trainer.ToString().ToLower(), "true"));
                            claims.Add(new Claim("weight", "true"));
                            break;
                        }
                    case UserRole.Client:
                        {
                            claims.Add(new Claim(UserRole.Client.ToString().ToLower(), "true"));
                            claims.Add(new Claim("weight", "true"));
                            break;
                        }
                    default: break;
                }

                if (user.IsActive)
                {
                    claims.Add(new Claim("active", "true"));
                }
            }

            try
            {
                var principle = new ClaimsPrincipal();
                principle.AddIdentity(new ClaimsIdentity(claims, "Password"));
                return principle;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}