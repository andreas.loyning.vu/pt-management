using AutoMapper;
using PTManagementAPI.Models.Domain;
using PTManagementAPI.Models.DTO;
using PTManagementAPI.Models.DTO.Booking;
using PTManagementAPI.Models.DTO.Client;
using PTManagementAPI.Models.DTO.InvoiceLine;
using PTManagementAPI.Models.DTO.Trainer;
using PTManagementAPI.Models.DTO.UserProfile;
using PTManagementAPI.Models.DTO.Venue;
using System.Linq;

namespace PTManagementAPI.Profiles
{
    public class PTDBProfile : Profile
    {
        public PTDBProfile()
        {
            //Venue Mappings
            CreateMap<VenueDTO, Venue>().ReverseMap();
            CreateMap<VenuePostDTO, Venue>().ReverseMap();

            CreateMap<InvoiceLineDTO, InvoiceLine>().ReverseMap();
            CreateMap<InvoiceLinePostDTO, InvoiceLine>().ReverseMap();
            CreateMap<BookingDTO, Booking>().ReverseMap();
            CreateMap<BookingPostDTO, Booking>().ReverseMap();

            // Trainer DTO's
            CreateMap<Trainer, TrainerProfileDTO>()
                .ForMember(dest => dest.InvoiceIds,
                    opt => opt.MapFrom(source => source.Invoices.Select(i => i.InvoiceId).ToList())
                )
                .ForMember(dest => dest.BookingIds,
                    opt => opt.MapFrom(source => source.Bookings.Select(i => i.BookingId).ToList())
                )
                .ForMember(dest => dest.ClientIds,
                    opt => opt.MapFrom(source => source.Clients.Select(i => i.ClientId).ToList())
                )
                .ReverseMap();
            CreateMap<UserDTO, User>().ReverseMap();
            CreateMap<UserDTO, TrainerProfileViewDTO>().ReverseMap();
            CreateMap<TrainerProfileDTO, TrainerProfileViewDTO>()
                .IncludeMembers(s => s.User)
                .ReverseMap();
            CreateMap<Client, TrainerClientDTO>().ReverseMap();
            CreateMap<User, TrainerClientDTO>().ReverseMap();
            // Post
            CreateMap<UserDTO, TrainerPostDTO>().ReverseMap();
            CreateMap<TrainerProfileDTO, TrainerPostDTO>()
                .IncludeMembers(s => s.User)
                .ReverseMap(); ;

            //Invoice Mappings
            // Mapping InvoiceLines to InvoiceLineIds
            // TODO: Maybe not map ids.
            CreateMap<Invoice, InvoiceGetDTO>()
                .ForMember(dest => dest.InvoiceLineIds,
                    opt => opt.MapFrom(source => source.InvoiceLines.Select(i => i.InvoiceLineId).ToList())
                ).ReverseMap();
            CreateMap<Invoice, InvoiceDetailsDTO>();
            CreateMap<InvoicePostDTO, Invoice>();
            // Weight mappings
            CreateMap<Weight, WeightGetDTO>().ReverseMap();
            CreateMap<WeightPostDTO, Weight>();

            CreateMap<Client, ClientProfileDTO>()
                .ForMember(dest => dest.FirstName, act => act.MapFrom(src => src.User.FirstName))
                .ForMember(dest => dest.MiddleName, act => act.MapFrom(src => src.User.MiddleName))
                .ForMember(dest => dest.LastName, act => act.MapFrom(src => src.User.LastName))
                .ForMember(dest => dest.DOB, act => act.MapFrom(src => src.User.DOB))
                .ForMember(dest => dest.Email, act => act.MapFrom(src => src.User.Email))
                .ForMember(dest => dest.Address, act => act.MapFrom(src => src.User.Address))
                .ForMember(dest => dest.TrainerFirstName, act => act.MapFrom(src => src.Trainer.User.FirstName))
                .ForMember(dest => dest.TrainerMiddleName, act => act.MapFrom(src => src.Trainer.User.MiddleName))
                .ForMember(dest => dest.TrainerLastName, act => act.MapFrom(src => src.Trainer.User.LastName))
                .ForMember(dest => dest.Image, act => act.MapFrom(src => src.User.Image))
                .ForMember(dest => dest.AdditionalInfo, act => act.MapFrom(src => src.User.AdditionalInfo))
                .ForMember(dest => dest.Phone, act => act.MapFrom(src => src.User.Phone))
                .ForMember(dest => dest.PostalCode, act => act.MapFrom(src => src.User.PostalCode))
                .ForMember(dest => dest.Role, act => act.MapFrom(src => src.User.Role))
                .ForMember(dest => dest.CreatedAt, act => act.MapFrom(src => src.User.CreatedAt))
                .ReverseMap();

            CreateMap<ClientPostDTO, Client>().ReverseMap();
            CreateMap<ClientPostDTO, User>().ReverseMap();

            CreateMap<ClientUpdateDTO, Client>().ReverseMap();
            CreateMap<ClientUpdateDTO, User>().ReverseMap();

            CreateMap<InvoiceLineDTO, InvoiceLine>().ReverseMap();

            //Admin mappings
            CreateMap<Admin, AdminDTO>()
                .ForMember(dest => dest.FirstName, act => act.MapFrom(src => src.User.FirstName))
                .ForMember(dest => dest.LastName, act => act.MapFrom(src => src.User.LastName))
                .ForMember(dest => dest.Image, act => act.MapFrom(src => src.User.Image))
                .ForMember(dest => dest.Role, act => act.MapFrom(src => src.User.Role))
                .ForMember(dest => dest.Email, act => act.MapFrom(src => src.User.Email))
                .ReverseMap();
        }
    }
}