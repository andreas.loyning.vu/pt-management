using System;
using System.IO;
using System.Reflection;
using Microsoft.AspNetCore.Authentication.Google;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Logging;
using Microsoft.OpenApi.Models;
using PdfSharpCore.Fonts;
using PTManagementAPI.Models.Domain;
using PTManagementAPI.Models.Email;
using PTManagementAPI.Services;
using PTManagementAPI.TokenValidators;
using PTManagementAPI.Utils.PdfUtils;

namespace PTManagementAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            
            services.AddControllersWithViews()
                    .AddNewtonsoftJson(options =>
            options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);
            services.AddDbContext<PTDBContext>(options => options.UseSqlServer(Configuration["ConnectionStrings:PTManagementDatabase"]));
            services.AddScoped<VenueService>();
            services.AddScoped<ClientService>();
            services.AddScoped<TrainerService>();
            services.AddScoped<InvoiceService>();
            services.AddScoped<InvoiceLineService>();
            services.AddScoped<BookingService>();
            services.AddScoped<WeightService>();
            services.AddScoped<MailService>();
            services.TryAddSingleton<HttpContextAccessor>();
            services.AddScoped<UserService>();
            services.AddAutoMapper(typeof(Startup));
            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "PTManagementAPI", Version = "v1" });
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;

            })
            .AddJwtBearer(o =>
                {
                    // Build an intermediate service provider
                    var sp = services.BuildServiceProvider();
                    // Resolve the services from the service provider
                    var userService = sp.GetService<UserService>();
                    o.SecurityTokenValidators.Clear();
                    o.SecurityTokenValidators.Add(new GoogleTokenValidator(userService));
                });
            services.AddAuthorization(config =>
            {
                config.AddPolicy("Admin",
                options => options.RequireClaim("admin"));

                config.AddPolicy("Trainer",
                options => options.RequireClaim("trainer"));

                config.AddPolicy("Client",
                options => options.RequireClaim("client"));

                config.AddPolicy("Active",
                options => options.RequireClaim("active"));

                config.AddPolicy("Weight",
                options => options.RequireClaim("weight"));
            });
            services.AddCors(c =>
            {
                c.AddPolicy("AllowOrigin", options => options.AllowAnyOrigin()
                                                             .AllowAnyMethod()
                                                             .AllowAnyHeader());
            });

            // Add email services
            services.Configure<SmtpSettings>(Configuration.GetSection("SmtpSettings"));
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                IdentityModelEventSource.ShowPII = true;
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "PTManagementAPI v1");
                c.RoutePrefix = string.Empty;
            });
            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();
            app.UseCors("AllowOrigin");

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
