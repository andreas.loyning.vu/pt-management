﻿using MigraDocCore.DocumentObjectModel;
using MigraDocCore.DocumentObjectModel.Tables;
using MigraDocCore.Rendering;
using PdfSharpCore.Fonts;
using PdfSharpCore.Pdf;
using PTManagementAPI.Models.Domain;
using System.Collections.Generic;

namespace PTManagementAPI.Utils.PdfUtils
{
    public static class PdfUtils
    {
        public static PdfDocument CreateDocument(Trainer trainer, Invoice invoice, Client client)
        {
            if (GlobalFontSettings.FontResolver is null)
            {
                GlobalFontSettings.FontResolver = new FontResolver();
            }

            ICollection<InvoiceLine> invoiceLines = invoice.InvoiceLines;
            // Create a new MigraDoc document
            Document document = new Document();

            Section section = document.AddSection();
            section.AddTextFrame();
            section.AddParagraph();



            // Paragraph for document description
            Paragraph paragraphIntro = section.AddParagraph();
            paragraphIntro.AddFormattedText("PT-management invoice", TextFormat.Bold);
            paragraphIntro.AddTab();
            paragraphIntro.AddDateField();
            paragraphIntro.AddLineBreak();
            paragraphIntro.AddLineBreak();
            paragraphIntro.AddFormattedText("About document", TextFormat.Bold);
            paragraphIntro.AddLineBreak();
            paragraphIntro.AddFormattedText("Invoice for purchase at PT-management AS. " +
                "                       This document contains information needed to make payment for any purchases.");
            paragraphIntro.AddLineBreak();
            paragraphIntro.AddLineBreak();
            // paragraph for cancellation policy
            Paragraph paragraphReturn = section.AddParagraph();
            paragraphReturn.AddFormattedText("Cancellation policy", TextFormat.Bold);
            paragraphReturn.AddLineBreak();
            paragraphReturn.AddFormattedText("We understand that some times, you just cant make a training session." +
                " Therefore, at PT-management AS, our policy states that a customer can cancel a booking 24-hours in advance for a full refund. " +
                " If you must cancel later, we must have for full payment.");
            paragraphReturn.AddLineBreak();
            paragraphReturn.AddLineBreak();
            // paragraph for support & contact


            Paragraph paragraphContact = section.AddParagraph();
            paragraphContact.AddFormattedText("Support", TextFormat.Bold);
            paragraphContact.AddLineBreak();
            paragraphContact.AddText("Should there be any problems with payment. Please contact us at pt-management.support@pt.com, or call us between 0800-1600 at: +47 00 00 00 00");
            paragraphContact.AddLineBreak();
            paragraphContact.AddLineBreak();

            Paragraph paragraphTables = section.AddParagraph();
            // Table for dates
            Table tableDates = new Table();

            tableDates.Borders.Width = 0.75;

            Column columnDates = tableDates.AddColumn(Unit.FromCentimeter(2));
            columnDates.Format.Alignment = ParagraphAlignment.Center;

            tableDates.AddColumn(Unit.FromCentimeter(5));
            Row rowDates = tableDates.AddRow();
            rowDates.Shading.Color = Colors.PaleGoldenrod;
            Cell cellDates = rowDates.Cells[0];
            cellDates.AddParagraph("Dates");
            cellDates = rowDates.Cells[1];
            cellDates.AddParagraph("");

            rowDates = tableDates.AddRow();
            cellDates = rowDates.Cells[0];
            cellDates.AddParagraph("Due Date");
            cellDates = rowDates.Cells[1];
            cellDates.AddParagraph(invoice.DueDate.ToString());

            rowDates = tableDates.AddRow();
            cellDates = rowDates.Cells[0];
            cellDates.AddParagraph("Issued");
            cellDates = rowDates.Cells[1];
            cellDates.AddParagraph(invoice.IssueDate.ToString());

            tableDates.SetEdge(0, 0, 2, 3, Edge.Box, BorderStyle.Single, 1.5, Colors.Black);

            paragraphTables.AddLineBreak();
            paragraphTables.AddLineBreak();

            // Table for client name, trainer name, more?
            Table tableClient = new Table().Clone();

            tableClient.Borders.Width = 0.75;

            Column column = tableClient.AddColumn(Unit.FromCentimeter(2));
            column.Format.Alignment = ParagraphAlignment.Center;

            tableClient.AddColumn(Unit.FromCentimeter(5));

            Row rowClient = tableClient.AddRow();
            rowClient.Shading.Color = Colors.PaleGoldenrod;
            Cell cellClient = rowClient.Cells[0];
            cellClient.AddParagraph("Client");
            cellClient = rowClient.Cells[1];
            cellClient.AddParagraph("");

            rowClient = tableClient.AddRow();
            cellClient = rowClient.Cells[0];
            cellClient.AddParagraph("First Name");
            cellClient = rowClient.Cells[1];
            cellClient.AddParagraph(client.User.FirstName);

            rowClient = tableClient.AddRow();
            cellClient = rowClient.Cells[0];
            cellClient.AddParagraph("Last Name");
            cellClient = rowClient.Cells[1];
            cellClient.AddParagraph(client.User.LastName);

            rowClient = tableClient.AddRow();
            cellClient = rowClient.Cells[0];
            cellClient.AddParagraph("Address");
            cellClient = rowClient.Cells[1];
            cellClient.AddParagraph(client.User.Address + ", " + client.User.PostalCode);

            rowClient = tableClient.AddRow();
            cellClient = rowClient.Cells[0];
            cellClient.AddParagraph("Phone");
            cellClient = rowClient.Cells[1];
            cellClient.AddParagraph(client.User.Phone);

            tableClient.SetEdge(0, 0, 2, 5, Edge.Box, BorderStyle.Single, 1.5, Colors.Black);

            paragraphTables.AddLineBreak();
            paragraphTables.AddLineBreak();


            // Table for client name, trainer name, more?
            Table tableTrainer = new Table().Clone();

            tableTrainer.Borders.Width = 0.75;

            Column columnTrainer = tableTrainer.AddColumn(Unit.FromCentimeter(2));
            columnTrainer.Format.Alignment = ParagraphAlignment.Center;

            tableTrainer.AddColumn(Unit.FromCentimeter(5));

            Row rowTrainer = tableTrainer.AddRow();
            rowTrainer.Shading.Color = Colors.PaleGoldenrod;
            Cell cellTrainer = rowTrainer.Cells[0];
            cellTrainer.AddParagraph("Trainer");
            cellTrainer = rowTrainer.Cells[1];
            cellTrainer.AddParagraph("");

            rowTrainer = tableTrainer.AddRow();
            cellTrainer = rowTrainer.Cells[0];
            cellTrainer.AddParagraph("First Name");
            cellTrainer = rowTrainer.Cells[1];
            cellTrainer.AddParagraph(trainer.User.FirstName);

            rowTrainer = tableTrainer.AddRow();
            cellTrainer = rowTrainer.Cells[0];
            cellTrainer.AddParagraph("Last Name");
            cellTrainer = rowTrainer.Cells[1];
            cellTrainer.AddParagraph(trainer.User.LastName);

            rowTrainer = tableTrainer.AddRow();
            cellTrainer = rowTrainer.Cells[0];
            cellTrainer.AddParagraph("Phone");
            cellTrainer = rowTrainer.Cells[1];
            cellTrainer.AddParagraph(trainer.User.Phone);

            tableTrainer.SetEdge(0, 0, 2, 4, Edge.Box, BorderStyle.Single, 1.5, Colors.Black);

            paragraphTables.AddLineBreak();
            paragraphTables.AddLineBreak();

            Table tablePurchaseDetails = CreateInvoiceLineTable(invoiceLines);


            document.LastSection.AddParagraph("");
            document.LastSection.Add(tableDates);
            document.LastSection.AddParagraph("");
            document.LastSection.Add(tableClient.Clone());
            document.LastSection.AddParagraph("");
            document.LastSection.Add(tableTrainer.Clone());
            document.LastSection.AddParagraph("");
            document.LastSection.Add(tablePurchaseDetails.Clone());

            // Table for item purchased, quantity, price -- purchase date?

            PdfDocumentRenderer pdfRenderer = new PdfDocumentRenderer(false);

            pdfRenderer.Document = document;
            pdfRenderer.RenderDocument();

            PdfDocument pdf = pdfRenderer.PdfDocument;

            pdf.Info.Title = "Invoice";
            return pdf;
        }

        public static Table CreateInvoiceLineTable(ICollection<InvoiceLine> invoiceLines)
        {
            Table table = new Table().Clone();

            table.Borders.Width = 0.25;

            Column column = table.AddColumn(Unit.FromCentimeter(2.2));
            column.Format.Alignment = ParagraphAlignment.Center;

            table.AddColumn(Unit.FromCentimeter(2.2));
            table.AddColumn(Unit.FromCentimeter(4));
            table.AddColumn(Unit.FromCentimeter(2.2));
            table.AddColumn(Unit.FromCentimeter(2.2));
            table.AddColumn(Unit.FromCentimeter(2.2));

            Row row = table.AddRow();
            row.Shading.Color = Colors.PaleGoldenrod;
            Cell cell = row.Cells[0];
            cell.AddParagraph("Invoice ID");
            cell = row.Cells[1];
            cell.AddParagraph("Line ID");
            cell = row.Cells[2];
            cell.AddParagraph("Description");
            cell = row.Cells[3];
            cell.AddParagraph("Quantity");
            cell = row.Cells[4];
            cell.AddParagraph("Price per item");
            cell = row.Cells[5];
            cell.AddParagraph("Price");
            decimal totalPrice = 0;
            int rowCount = 2;
            foreach (InvoiceLine line in invoiceLines)
            {
                // price per quantity * quantity
                totalPrice += (line.Price * (decimal)line.Quantity);
                rowCount += 1;

                row = table.AddRow();
                cell = row.Cells[0];
                cell.AddParagraph(line.InvoiceId.ToString());
                cell = row.Cells[1];
                cell.AddParagraph(line.InvoiceLineId.ToString());
                cell = row.Cells[2];
                cell.AddParagraph(line.Description);
                cell = row.Cells[3];
                cell.AddParagraph(line.Quantity.ToString());
                cell = row.Cells[4];
                cell.AddParagraph(line.Price.ToString());
                cell = row.Cells[5];
                cell.AddParagraph(totalPrice.ToString());


            }

            row = table.AddRow();
            cell = row.Cells[4];
            cell.AddParagraph("Total price");
            cell = row.Cells[5];
            cell.AddParagraph(totalPrice.ToString());

            table.SetEdge(0, 0, 6, rowCount, Edge.Box, BorderStyle.Single, 1.5, Colors.Black);

            return table;
        }

    }




}

