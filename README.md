# PT Management
### Created by Andreas Vu, Bao Nguyen, Muneeb Rana, Olav Rongved
API documentation is created with Swagger and can be accessed via [PT Management Swagger](https://ptmanagementapi20210405122623.azurewebsites.net/index.html)

## Features
The features present are divided into 3 categories which correspond to the 3 roles present in the application. These are:
1. Admin
2. Trainer
3. Client

### 1. Admin
Admin functionality has the most extensive functionality present in the application. Features and functionality are present below and are divided based on buttons present in the sidebar menu when logged in as an admin.
#### Admin Dashboard
Admin dashboard is the default page where an admin lands once logged in. The dashboard contains the following information:
##### Information Cards
- The total number of clients present
- The total number of trainers present
- The total number of Venues
- Newly registered trainers
- Newly registered clients

##### Pending Trainers List
This list contains all trainers that have registered to the system but have not yet been accepted by the admin. The admin can accept or reject the trainers through here. 

##### Total Sales Graph
This graph shows the total sales made by all trainers via the system. The graph has 5 presets where each specificy a time range which can help filter sales. The ranges are mentioned below
- Last Week
- Last Month
- Last Year
- Last 5 Years
- All Time

#### Trainers
This section contains all the trainers present in the system. From here, the admin can easily view the important trainer information and if needed, update them as well. 

#### Clients
This sections contains all clients present in the system. An admin can view clients by clicking on the small icon present under the "Go To" heading. This will take the admin to subsequent client's profile where the admin can update information, add weight, height etc.

#### Bookings
The bookings section shows a calendar view, defaulted to the current month. The view can be changed to the 4 following options
1. Day 
2. Week
3. Month
4. Four Days

All the bookings for all trainers will reside on this calendar. Here the admin can add new bookings by first choosing the desired date, and then the trainer. Based on the selected trainer, the trainer's clients will be shown, thus removing the hassle of selecting the correct trainer, client pairs. 

Once the trainer, client pair has been selected, the admin can choose venue, time and other information before sending in the booking. The booking will then be presented on the trainer's calendar as well. 

When it comes to updating, only an admin can update all kinds of bookings (i.e Booked, Cancelled, Completed) while trainer can only update bookings where the booking status is "Booked". Once the booking status changes to "Compeleted" or "Cancelled", only the admin has the rights to change update those bookings. 

#### Venues
This section contains all the venues present in the system. Here the admin can view, add or update venues.

#### Invoices
Invoices section is divided into two parts
1. All Invoices
This part will contain all invoices for all trainers.
2. Add Invoice
Here the admin can send invoices via a trainer to the trainer's clients. It is important to remember that each invoice line added must have price and quantity.

#### Report
The report section will contain multiple categories that can be reported to provide different kinds of stats. Currently only invoice reporting has been implemented. 

##### Invoice Report
Invoices can be reported by first selecting either a date range, or a date preset. The following date presents can be found
1. Last Month
2. Last 6 Months
3. This Year
4. All Time

After that there is an option between choosing to show invoices for all trainers, or a specific trainer. If all trainers preset is chosen, the invoice will show invoices for all clients. However if a specific trainer is chosen, the admin can then decide which client of that specific trainer should be selected. 

Once all the selections are decided, the generate invoice button can be clicked to generate an invoice table. The values in the invoice table can be then exported as CSV if required. 

### 2. Trainer
The trainer has less functionality compared to the admin and only has access to its own clients.

#### Trainer Dashboard
The trainer dashboard is the landing page once a trainer logs in. It contains the following functionality and information:
##### Information Cards
- Total clients
- Total clients last month
##### Clients
This table contains all clients for the trainer. The clients are color coded in the following manner:
1. Number of sessions = 0 | Red
2. Number of sessions = 1 | Yellow
3. Number of sessions >= 2 && Number of sessions <=4 | Blue
4. Number of sessions >= 5 | Green

This helps trainer identify which clients to target and sell more to.

##### Bookings
Calendar where a trainer can add and update bookings for their clients 

#### Clients
A section where trainer can view all their clients

#### Venues 
See Admin section

#### Invoices
Similar to admin, trainer can send invoices to clients here. The only difference is that trainer can only view their own clients.

#### Report
In this section, a trainer can decide whether to generate invoice report for all clients or a specified client. The date range and presets mentioned in Admin Report section applies.

### 3. Client
Client when logged in are directed to the client dashboard. This dashboard is the only page they can view since the client functionality on the application is minimal. 

#### Client Dashboard
Here the client can view and update their general information and change their weight based on their progress. The weight changes are shown in graph form to the client. 
## Build/Run the Application
- A connection string is required to access the 
- Once the connection string is present, migrations must be run to create the database.
- SMTP information is required for sending emails.
- The connection string and SMTP information can either be placed in appsettings.json or can be provided as environment variables. 
- If using environment variables, the following format will work: 
> ConnectionStrings:[Database Name]="" SmtpSettings:Server="" SmtpSettings:Port="" SmtpSettings:SenderName="" SmtpSettings:SenderEmail="" SmtpSettings:Username="" SmtpSettings:Password=""